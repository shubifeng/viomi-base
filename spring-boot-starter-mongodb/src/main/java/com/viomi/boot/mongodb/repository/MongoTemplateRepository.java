package com.viomi.boot.mongodb.repository;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.viomi.base.model.dto.page.PageResult;
import com.viomi.base.model.dto.page.Pager;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.convert.DefaultMongoTypeMapper;
import org.springframework.data.mongodb.core.convert.MappingMongoConverter;
import org.springframework.data.mongodb.core.mapreduce.MapReduceResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.Date;
import java.util.List;

//cant use DetachedCriteria
@SuppressWarnings("unchecked")
public abstract class MongoTemplateRepository {
    protected final Logger logger = LoggerFactory.getLogger(getClass());

    //    @Autowired
    private MongoTemplate mongoTemplate;

    /**
     * 实体bean对象转换成DBObject
     *
     * @param bean
     * @return
     * @throws IllegalArgumentException
     * @throws IllegalAccessException
     */
    private static <T> DBObject bean2DBObject(T bean) throws IllegalArgumentException,
            IllegalAccessException {
        if (bean == null) {
            return null;
        }
        DBObject dbObject = new BasicDBObject();
        // 获取对象对应类中的所有属性域
        Field[] fields = bean.getClass().getDeclaredFields();
        for (Field field : fields) {
            // 获取属性名
            String varName = field.getName();
            // 修改访问控制权限
            boolean accessFlag = field.isAccessible();
            if (!accessFlag) {
                field.setAccessible(true);
            }
            Object param = field.get(bean);
            if (param == null) {
                continue;
            } else if (param instanceof Integer) {
                // 判断变量的类型
                int value = ((Integer) param).intValue();
                dbObject.put(varName, value);
            } else if (param instanceof String) {
                String value = (String) param;
                dbObject.put(varName, value);
            } else if (param instanceof Double) {
                double value = ((Double) param).doubleValue();
                dbObject.put(varName, value);
            } else if (param instanceof Float) {
                float value = ((Float) param).floatValue();
                dbObject.put(varName, value);
            } else if (param instanceof Long) {
                long value = ((Long) param).longValue();
                dbObject.put(varName, value);
            } else if (param instanceof Boolean) {
                boolean value = ((Boolean) param).booleanValue();
                dbObject.put(varName, value);
            } else if (param instanceof Date) {
                Date value = (Date) param;
                dbObject.put(varName, value);
            } else if (param instanceof Byte) {
                Byte value = (Byte) param;
                dbObject.put(varName, value);
            } else if (param instanceof BasicDBObject) {
                BasicDBObject value = (BasicDBObject) param;
                dbObject.put(varName, value);
            } else if (param instanceof BasicDBList) {
                BasicDBList value = (BasicDBList) param;
                dbObject.put(varName, value);
            } else {
                throw new IllegalArgumentException("Cant handle param for instanceof:" + param.getClass());
            }
            // 恢复访问控制权限
            field.setAccessible(accessFlag);
        }
        return dbObject;
    }

    /**
     * DBObject转实体bean对象
     *
     * @param dbObject
     * @param bean
     * @return
     * @throws IllegalAccessException
     * @throws java.lang.reflect.InvocationTargetException
     * @throws NoSuchMethodException
     */
    private static <T> T dbObject2Bean(DBObject dbObject, T bean) throws IllegalAccessException,
            InvocationTargetException, NoSuchMethodException {
        if (bean == null) {
            return null;
        }
        Field[] fields = bean.getClass().getDeclaredFields();
        for (Field field : fields) {
            String varName = field.getName();
            Object object = dbObject.get(varName);
            if (object != null) {
                BeanUtils.setProperty(bean, varName, object);
            }
        }
        return bean;
    }

    protected abstract <T> Class<T> getEntityClass();

    @Autowired
//    @Qualifier("mongoTemplate")
    public void setMongoTemplate(MongoTemplate mongoTemplate) {
        //去掉自动添加_class
        ((MappingMongoConverter) mongoTemplate.getConverter()).setTypeMapper(new DefaultMongoTypeMapper(null));
        this.mongoTemplate = mongoTemplate;

//        MongoDbFactory factory = new SimpleMongoDbFactory(new MongoClient("10.0.75.1") , this.getEntityClass().getSimpleName());
//        MappingMongoConverter converter = new MappingMongoConverter(factory, new MongoMappingContext());
//        converter.setTypeMapper(new DefaultMongoTypeMapper(null));
//        this.mongoTemplate = new MongoTemplate(factory, converter);
    }

    /**
     * 特殊说明：
     * 1. 如果已经存在主键，则save(遍历列表,相当于upsert)会调用update更新记录，而insert(不用遍历，效率高)会忽略操作
     * 2. 如果entity结构改变，则会按照新的结构保存数据（Schema无关性）；
     */
    public <T> void save(T entity) {
        mongoTemplate.save(entity);
//        mongoTemplate.insert(entity);
    }

    public <T, E> T findById(E id) throws Exception {
        return (T) mongoTemplate.findById(id, this.getEntityClass());
    }

    public <T> T findOne(Query query) throws Exception {
        return (T) mongoTemplate.findOne(query, this.getEntityClass());
    }

    public long countByCondition(Query query) throws Exception {
        return mongoTemplate.count(query, this.getEntityClass());
    }

    public <T> T findListByCondition(Query query) throws Exception {
        return (T) mongoTemplate.find(query, this.getEntityClass());
    }

    public <T> void update(T entity) throws Exception {
        String idLabel = "id";
        String idValue = BeanUtils.getProperty(entity, idLabel);
        if (StringUtils.isNotBlank(idValue)) {
            //id value must be long for Query
            Query query = Query.query(Criteria.where(idLabel).is(Long.valueOf(idValue)));
            Update update = Update.fromDBObject(bean2DBObject(entity), idLabel);
            mongoTemplate.updateFirst(query, update, this.getEntityClass());
        }
    }

    public <T> void delete(T entity) throws Exception {
//      .remove(query, entityClass);
        mongoTemplate.remove(entity);
    }

    /**
     * @param id
     * @param filedName
     * @param filedValue
     * @throws Exception
     * @author：luocj
     * @createtime ： 2017年12月5日 上午9:39:08
     * @description 根据id 和 fieldName累加
     * @since version 初始于版本 v0.0.1
     */
    public void increaseValueToFiled(Object id, String filedName, Long filedValue) throws Exception {
        mongoTemplate.updateFirst(Query.query(Criteria.where("id").is(id)), new Update().inc(filedName, filedValue), this.getEntityClass());
    }

    /**
     * @param id
     * @param filedName
     * @param filedValue
     * @throws Exception
     * @author：luocj
     * @createtime ： 2017年12月4日 上午10:10:42
     * @description 根据ID 添加字段
     * 方法二：直接在实体对象增加字段，调用保存方法
     * @since version 初始于版本 v0.0.1
     */
    public void addFiled(Object id, String filedName, String filedValue) throws Exception {
//        super.update(Query.query(Criteria.where("id").is(id)), Update.update(filedName, filedValue));
        mongoTemplate.updateFirst(Query.query(Criteria.where("id").is(id)), new Update().set(filedName, filedValue), this.getEntityClass());
    }

    /**
     * @param id
     * @param filedName
     * @throws Exception
     * @author：luocj
     * @createtime ： 2017年12月4日 上午10:41:04
     * @description 根据ID 删除字段
     * @since version 初始于版本 v0.0.1
     */
    public void deleteFiled(Object id, String filedName) throws Exception {
        mongoTemplate.updateFirst(Query.query(Criteria.where("id").is(id)), new Update().unset(filedName), this.getEntityClass());
    }

    /**
     * @param id
     * @param filedName
     * @param newFiledName
     * @throws Exception
     * @author：luocj
     * @createtime ： 2017年12月4日 上午11:48:16
     * @description 重命名字段名称
     * @since version 初始于版本 v0.0.1
     */
    public void renameFiled(Object id, String filedName, String newFiledName) throws Exception {
        mongoTemplate.updateFirst(Query.query(Criteria.where("id").is(id)), new Update().rename(filedName, newFiledName), this.getEntityClass());
    }

    /**
     * @param query
     * @param pager
     * @return
     * @throws Exception
     * @author：luocj
     * @createtime ： 2017年12月5日 下午5:20:51
     * @description 按条件查询多条数据, 带分页条件（分页从0开始）
     * @since version 初始于版本 v0.0.1
     */
    public <T> PageResult<T> findListByCondition(Query query, Pager pager) throws Exception {
        if (pager == null) {
            pager = new Pager(0, 2000);
        }

        long count = countByCondition(query);
        pager.setTotalRow((int) count);
        int pageSize = pager.getRowsPerPage();
        int totalPageNum = pager.getTotalPage();
        int nowPage = pager.getCurrentPage();

        query.skip((nowPage - 1) * pageSize); //start index
        query.limit(pageSize);
        List<T> list = findListByCondition(query);
        return new PageResult<T>(nowPage, pageSize, count, totalPageNum, list);
    }

    @SuppressWarnings("rawtypes")
    /**
     *
     * @author：luocj
     * @createtime ： 2017年12月29日 上午11:39:48
     * @description 聚合函数 限制mongoV3.4
     *              Changed in version 3.6: MongoDB 3.6 removes the use of aggregate command without the cursor option
     *              unless the command includes the explain option.
     * @since version 初始于版本 v0.0.1
     * @param aggregation
     * @param inputType
     * @param outputType
     * @return
     */
    public <T> T aggregate(Aggregation aggregation, Class inputType, Class outputType) {
        return (T) mongoTemplate.aggregate(aggregation, inputType, outputType);
    }

    /**
     * @param query
     * @param mapFunction
     * @param reduceFunction
     * @param outputEntityClass
     * @return
     * @author：luocj
     * @createtime ：2018年1月2日 下午18:00:55
     * @description mapReduce
     */
    public <T> MapReduceResults<T> mapReduce(Query query, String mapFunction, String reduceFunction, Class<T> outputEntityClass) {
        return mongoTemplate.mapReduce(query, getCollectionName(this.getEntityClass().getSimpleName()), mapFunction, reduceFunction, outputEntityClass);
    }

    private String getCollectionName(String simpleName) {
        if (null == simpleName || simpleName.length() == 0) {
            return "";
        }
        return simpleName.toLowerCase().substring(0, 1) + simpleName.substring(1);
    }

    public void testFnc() {
//        mongoTemplate.aggregate(aggregation, outputType)
//        mongoTemplate.group(inputCollectionName, groupBy, entityClass)
//        mongoTemplate.mapReduce(inputCollectionName, mapFunction, reduceFunction, entityClass)
    }
}
