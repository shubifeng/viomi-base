package com.viomi.boot.mongodb;

import com.viomi.base.model.mongodb.MongodbGeneratedValue;
import com.viomi.base.model.mongodb.Sequences;
import org.springframework.context.event.EventListener;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.mapping.event.AbstractMongoEventListener;
import org.springframework.data.mongodb.core.mapping.event.BeforeConvertEvent;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.util.ReflectionUtils;

import javax.annotation.Resource;
import java.lang.reflect.Field;

public class SaveMongoEventListener extends AbstractMongoEventListener<Object> {
    @Resource
    private MongoTemplate mongoTemplate;

    //  below version 1.7.2.RELEASE
//  public void onBeforeConvert(final Object source) {
    @Override
    @EventListener
    public void onBeforeConvert(BeforeConvertEvent<Object> event) {
        final Object source = event.getSource();
        if (source != null) {
            ReflectionUtils.doWithFields(source.getClass(), new ReflectionUtils.FieldCallback() {
                public void doWith(Field field) throws IllegalArgumentException, IllegalAccessException {
                    ReflectionUtils.makeAccessible(field);
                    if (field.isAnnotationPresent(MongodbGeneratedValue.class)) {
                        field.set(source, getNextSequenceId(source.getClass().getSimpleName()));
                    }
//                  if (field.isAnnotationPresent(TemporalValue.class)) {
////                      field.set(source, new SimpleDateFormat("yyyy-MM-dd").format((Date) field.get(source)));
////                      field.set(source, DateUtils.getDateBeginTime((Date) field.get(source)));
//                  }
                }
            });
        }
    }

    /**
     * @param id
     * @return
     * @author：luocj
     * @createtime ： 2017年12月5日 下午3:06:14
     * @description 通过维护一个文档储存序列，自增获得下一个sequenceId
     * @since version 初始于版本 v0.0.1
     */
    private Long getNextSequenceId(String id) {
        Query query = new Query(Criteria.where("id").is(id));
        Update update = new Update().inc("sequenceId", 1);//自增1
        FindAndModifyOptions options = new FindAndModifyOptions();
        options.upsert(true);
        options.returnNew(true);
        Sequences seqId = mongoTemplate.findAndModify(query, update, options, Sequences.class);
        return seqId.getSequenceId();
    }

}
