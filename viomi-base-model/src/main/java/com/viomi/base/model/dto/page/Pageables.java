package com.viomi.base.model.dto.page;

import lombok.Data;

import java.io.Serializable;

/**
 * @author ShuBifeng
 * @date 2018-01-04
 */
@Data
public class Pageables implements Serializable {
    /**
     * 默认页码
     */
    public static final int DEFAULT_PAGE_NUMBER = 1;
    /**
     * 默认每页记录数
     */
    public static final int DEFAULT_PAGE_SIZE = 20;
    /**
     * 最大每页记录数
     */
    public static final int MAX_PAGE_SIZE = 1000;
    private static final long serialVersionUID = -3930180379790344299L;
    /**
     * 页码
     */
    private int pageNum = DEFAULT_PAGE_NUMBER;

    /**
     * 每页记录数
     */
    private int pageSize = DEFAULT_PAGE_SIZE;

    /**
     * 搜索属性
     */
    private String searchProperty;

    /**
     * 搜索值
     */
    private String searchValue;

    /**
     * 查询周期的属性
     */
    private String searchCycleProperty;

    /**
     * 查询周期：0 最近一周； 1 最近一个月 ；2 最近三个月 ；3 最近一年
     */
    private Integer searchCycleValue;

    /**
     * 包含isCount查询
     */
    private Boolean isCount;

    /**
     * 排序属性
     */
    private String orderProperty;
/*

    */
/**
 * 排序方向
 *//*

    private Order.Direction orderDirection;

    */
/**
 * 筛选
 *//*

    private List<Filter> filters = new ArrayList<Filter>();

    */
/**
 * 排序
 *//*

    private List<Order> orders = new ArrayList<Order>();
*/

    /**
     * 构造方法
     */
    public Pageables() {
    }

    /**
     * 构造方法
     *
     * @param pageNum  页码
     * @param pageSize 每页记录数
     */
    public Pageables(Integer pageNum, Integer pageSize) {
        if (pageNum != null && pageNum >= 1) {
            this.pageNum = pageNum;
        }
        if (pageSize != null && pageSize >= 1 && pageSize <= MAX_PAGE_SIZE) {
            this.pageSize = pageSize;
        }
    }


}
