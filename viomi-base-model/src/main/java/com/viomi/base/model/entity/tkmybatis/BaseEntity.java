package com.viomi.base.model.entity.tkmybatis;

import lombok.Data;

import javax.persistence.Transient;
import java.io.Serializable;

/**
 * 基础类
 *
 * @author ShuBifeng
 * @date 2018-01-04
 */
@Data
public class BaseEntity<T, E> implements Serializable {

    @Transient
    private T id;
}
