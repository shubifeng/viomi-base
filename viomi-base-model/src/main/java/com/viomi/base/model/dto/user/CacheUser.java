package com.viomi.base.model.dto.user;


import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@NoArgsConstructor
public class CacheUser implements Serializable {

    /**
     * 账号类型： 1:商城账号  2:crm系统账号  3:通用账号
     */
    private Integer accountType;

    /**
     * 获取登录信息方式： 1:密码登录, 2:微信授权, 3:小米账号登录
     */
    private Integer loginType;

    /**
     * 对应数据表主键： Vmall vstore.vstore_user_baseinfo.id | Crm vm_auth.user_id
     */
    private Long userId;

    /**
     * 用户角色
     */
    private List<String> roles;

    /**
     * 商城用户 id
     */
    private Long storeUserId;
    /**
     * 代理人 id
     */
    private Long agentId;
    /**
     * 人员所属渠道id
     */
    private Long channelId;
    /**
     * 人员所属渠道名称
     */
    private String channelName;
    /**
     * 用户所属组织机构id
     */
    private Long orgId;
    /**
     * 用户所属组织机构名称
     */
    private String orgName;
    /**
     * 权限用户id
     */
    private Integer authUserId;
    /**
     * 后台用户账户编码
     */
    private String authUserAccount;
    /**
     * 后台用户名称
     */
    private String authUserName;
    /**
     * 用户微信昵称
     */
    private String wechatNickName;
    /**
     * 用户绑定的手机号码
     */
    private String mobile;

}
