package com.viomi.base.model.dto.req;

import io.swagger.annotations.ApiModelProperty;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

/**
 * @author ShuBIfeng
 */
@NoArgsConstructor
@Slf4j
@ToString
public class PageReq implements Serializable {

    /**
     * 为了防止出现分页性能问题限制：最大可查询页数
     */
    private final int maxLimitPageNum = 1000;
    /**
     * 为了防止出现分页性能问题限制：最大查询行数
     */
    private final int maxLimitPageSize = 200;

    @NotNull
    @Setter
    @ApiModelProperty(value = "页码", required = true)
    @Range(min = 1, max = 1000, message = "页码范围必须是 1 至 1000")
    private Integer pageNum;
    @Setter
    @ApiModelProperty(value = "每页行数", required = true)
    @Range(min = 1, max = 200, message = "行数范围必须是 1 至 200")
    private Integer pageSize;

    public PageReq(Integer pageNum, Integer pageSize) {
        this.pageNum = pageNum;
        this.pageSize = pageSize;
    }

    public Integer getPageNum() {
        if (Objects.nonNull(pageNum) && pageNum > maxLimitPageNum) {
            pageNum = maxLimitPageNum;
        }
        return pageNum;
    }

    public Integer getPageSize() {
        if (Objects.nonNull(pageSize) && pageSize > maxLimitPageSize) {
            pageSize = maxLimitPageSize;
        }
        return pageSize;
    }
}
