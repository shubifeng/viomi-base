package com.viomi.boot.xmapus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class XmApuApplication {
	public static void main(String[] args) {
		SpringApplication.run(XmApuApplication.class, args);
	}
}
