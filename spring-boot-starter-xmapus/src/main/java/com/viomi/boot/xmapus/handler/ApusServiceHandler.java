package com.viomi.boot.xmapus.handler;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

/**
 * 
 * @author：xrn
 * @createtime ： 2018年3月23日 上午10:34:24
 * @description 一句话描述
 * @since version 初始于版本
 */
@Component
public class ApusServiceHandler {
	private static ApplicationContext ctx;
	private static Map<String, Object> serviceMap = new HashMap<String, Object>();

	/**
	 * 
	 * @author：xrn
	 * @createtime ： 2018年3月23日 下午2:07:09
	 * @description 一句话描述
	 * @since version 初始于版本
	 * @param ctx
	 * @throws BeansException
	 */
	public static void setApplicationContext(ApplicationContext ctx) {
		ApusServiceHandler.ctx = ctx;
	}

	/**
	 * 
	 * @author：xrn
	 * @createtime ： 2018年3月23日 上午11:51:27
	 * @description 一句话描述
	 * @since version 初始于版本
	 * @return
	 */
	public Object getServiceInstance(String serviceName) {
		return serviceMap.get(serviceName);
	}

	/**
	 * 
	 * @param <T>
	 * @author：xrn
	 * @createtime ： 2018年3月23日 下午12:52:58
	 * @description 一句话描述
	 * @since version 初始于版本
	 * @param className
	 */
	public static <T> void addSpecifiedInvokedService(Class<T> className) {
		if (!serviceMap.containsKey(className.getSimpleName())) {
			T bean = ctx.getBean(className);
			if (bean != null) {
				serviceMap.put(className.getSimpleName(), bean);
			}
		}
	}
}
