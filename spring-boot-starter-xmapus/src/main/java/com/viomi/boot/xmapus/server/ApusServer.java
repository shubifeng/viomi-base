package com.viomi.boot.xmapus.server;

import java.util.List;

import org.springframework.context.ApplicationContext;

import com.facebook.swift.service.ThriftMethod;
import com.facebook.swift.service.ThriftService;
import com.viomi.boot.xmapus.handler.ApusServiceHandler;
import com.viomi.boot.xmapus.server.impl.ApusServerImpl;
import com.xiaomi.apus.metrics.OpenFalconMetricsFactory;
import com.xiaomi.apus.registry.servicecenter.ServiceCenterRegistryFactory;
import com.xiaomi.apus.server.Bootstrap;
import com.xiaomi.apus.server.ServerConfig;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ApusServer {

	@ThriftService("ExpressApusServer")
	public interface Iface {
		@ThriftMethod("execute")
		String execute(String paramsJson);
	}

	public static void start(ApplicationContext ctx, List<Class<?>> specifiedInvokedServices, int port) {
		ApusServiceHandler.setApplicationContext(ctx);
		specifiedInvokedServices.forEach(e -> ApusServiceHandler.addSpecifiedInvokedService(e));
		ServerConfig<ApusServerImpl> config = new ServerConfig.Builder<>(ApusServerImpl.class)
				.registryFactory(new ServiceCenterRegistryFactory<>(ApusServerImpl.class))
				.metricsFactory(new OpenFalconMetricsFactory("method", '/')).port(port).build();

		Bootstrap<ApusServerImpl> bootstrap = new Bootstrap<>(config);
		bootstrap.start();
		log.info("apus server star suceessfully, port: {}", port);
		System.out.println("apus server star suceessfully, port:" + port);
	}
}
