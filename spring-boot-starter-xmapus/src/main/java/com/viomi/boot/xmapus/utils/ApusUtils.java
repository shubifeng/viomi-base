/**
 * @author：xrn
 * @createtime ： 2018年3月23日 上午9:29:25
 * @description TODO 一句话描述
 */
package com.viomi.boot.xmapus.utils;

import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;

import com.alibaba.fastjson.JSON;

/**
 * @author：xrn
 * @createtime ： 2018年3月23日 上午9:29:25
 * @description 一句话描述
 * @since version 初始于版本
 */
public class ApusUtils {
	// 每次请求都将当前用户登录信息保存到当前线程变量中，方便处理业务时获取，如果在处理业务时另起线程，则需要将登录信息以参数形式传给另一个线程
	public static ThreadLocal<Map<String, Object>> headerInfo = new ThreadLocal<Map<String, Object>>();
	public static Map<String, Object> paramsMap;

	@SuppressWarnings("unchecked")
	public static Map<String, Object> parseParamMap(String paramsJson) {
		paramsMap = JSON.parseObject(paramsJson, Map.class);
		return paramsMap;
	}

	@SuppressWarnings("unchecked")
	public static Map<String, Object> getHeader() {
		return MapUtils.getMap(paramsMap, "params");
	}

	@SuppressWarnings("unchecked")
	public static Map<String, Object> getParams() {
		return MapUtils.getMap(paramsMap, "params");
	}

	public static String getMethod() {
		return MapUtils.getString(paramsMap, "method", "");
	}

	public static String getServiceSimpleName() {
		String method = getMethod();
		String serviceName = method.substring(0, method.lastIndexOf("."));
		return serviceName.substring(serviceName.lastIndexOf(".") + 1);
	}

	public static String getMethodName() {
		String method = getMethod();
		return method.substring(method.lastIndexOf(".") + 1);
	}

	public static Map<String, Object> getThreadHeaderInfo() {
		return headerInfo.get();
	}

	public static void setThreadHeaderInfo(Map<String, Object> header) {
		headerInfo.set(header);
	}

	public static String getUserId() {
		String userId = "";
		if (headerInfo.get() != null && !headerInfo.get().isEmpty()) {
			userId = MapUtils.getString(headerInfo.get(), "userId");
		}
		return !StringUtils.isBlank(userId) ? userId : "sys";
	}
}
