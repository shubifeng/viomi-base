package com.viomi.boot.xmapus.handler;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import com.viomi.boot.xmapus.utils.ApusUtils;

/**
 * 
 * @author：xrn
 * @createtime ： 2018年3月23日 上午9:28:41
 * @description
 * @since version 初始于版本
 */
public class ApusMethodHandler {
	// 要执行的方法对象
	private Object object;

	// 要执行的方法名称
	private String methodName;

	// 要执行方法的参数
	private Map<String, Object> params;

	private Map<String, Object> header;

	private Map<String, Object> methodMap = new HashMap<String, Object>();

	/**
	 * 构造方法
	 * 
	 * @param object
	 * @param methodName
	 * @param params
	 */
	public ApusMethodHandler(Object object, String methodName, Map<String, Object> header, Map<String, Object> params) {
		super();
		this.object = object;
		this.methodName = methodName;
		this.header = header;
		this.params = params;
	}

	/**
	 * 
	 * @throws SecurityException
	 * @throws NoSuchMethodException
	 * @throws InvocationTargetException
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 * @author：xrn
	 * @createtime ： 2018年3月23日 上午9:26:35
	 * @description 调用具体方法
	 * @since version 初始于版本
	 */
	public String invoke() throws NoSuchMethodException, SecurityException, IllegalAccessException,
			IllegalArgumentException, InvocationTargetException {
		Method method;
		if (methodMap.containsKey(methodName)) {
			method = (Method) methodMap.get(methodName);
		} else {
			method = object.getClass().getMethod(methodName, Map.class, Map.class);
			methodMap.putIfAbsent(methodName, method);
		}
		ApusUtils.setThreadHeaderInfo(header);
		return (String) method.invoke(object, header, params);
	}
}
