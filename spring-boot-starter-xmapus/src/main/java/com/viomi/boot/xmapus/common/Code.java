package com.viomi.boot.xmapus.common;

public interface Code {

	Integer getCode();

	String getDesc();
}
