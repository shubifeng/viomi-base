package com.viomi.boot.xmapus.client;

import com.viomi.boot.xmapus.server.ApusServer;
import com.xiaomi.apus.ClientConfig;
import com.xiaomi.apus.SwiftClientManager;
import com.xiaomi.apus.registry.servicecenter.ServiceCenterRegistryFactory;

public class ApusClient {
	public static ApusServer.Iface getExpressApusSyncClient() {
		System.out.println("=============== client开始调用 ===============");
		ClientConfig<ApusServer.Iface> expressSyncClientConfig = new ClientConfig.ClientConfigBuilder<>(
				ApusServer.Iface.class).registryFactory(new ServiceCenterRegistryFactory<>(ApusServer.Iface.class))
						.build();
		return new SwiftClientManager<>(expressSyncClientConfig).createClient();
	}
}
