package com.viomi.boot.xmapus.common;

import java.io.Serializable;

public class ResultResp<T> implements Serializable {

	private static final long serialVersionUID = 7712593181650336137L;

	protected Integer code;

	protected String desc;

	protected Long timestamp;

	protected T result;

	private ResultResp(Integer code, String desc, T result) {
		this.code = code;
		this.desc = desc;
		this.result = result;
	}

	private ResultResp(Integer code, String desc, T result, Long timestamp) {
		this.code = code;
		this.desc = desc;
		this.result = result;
		this.timestamp = timestamp;
	}

	/**
	 * 正常返回（无业务数据返回）
	 *
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static <T> ResultResp<T> ok() {
		return new ResultResp(CommRespCode.SUCCESS.getCode(), CommRespCode.SUCCESS.getDesc(), null);
	}

	/**
	 * 正常返回（有业务数据返回）
	 *
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static <T> ResultResp<T> ok(T result) {
		return new ResultResp(CommRespCode.SUCCESS.getCode(), CommRespCode.SUCCESS.getDesc(), result);
	}

	/**
	 * 正常返回（自定义返回码，并返回业务数据）
	 *
	 * @param code
	 * @param result
	 *            无需业务数据返回时请填写null
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static <T> ResultResp<T> ok(Code code, T result) {
		return new ResultResp(code.getCode(), code.getDesc(), result);
	}

	/**
	 * 正常返回（自定义返回信息，并返回业务数据）
	 *
	 * @param msg
	 * @param result
	 *            无需业务数据返回时请填写null
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static <T> ResultResp<T> ok(String msg, T result) {
		return new ResultResp(CommRespCode.SUCCESS.getCode(), msg, result);
	}

	/**
	 * 默认通用的错误返回
	 *
	 * @return
	 */
	public static <T> ResultResp<T> fail() {
		return fail(CommRespCode.FAIL);
	}

	/**
	 * 默认通用的错误返回（返回编码使用默认编码，错误信息重写）
	 *
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static <T> ResultResp<T> fail(String msg) {
		return new ResultResp(CommRespCode.FAIL.getCode(), msg, null, System.currentTimeMillis());
	}

	/**
	 * 错误返回（自定义）
	 *
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static <T> ResultResp<T> fail(Code code) {
		return new ResultResp(code.getCode(), code.getDesc(), null, System.currentTimeMillis());
	}

	/**
	 * 错误返回（自定义）
	 *
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static <T> ResultResp<T> fail(Code code, String msg) {
		return new ResultResp(code.getCode(), msg, null, System.currentTimeMillis());
	}
}
