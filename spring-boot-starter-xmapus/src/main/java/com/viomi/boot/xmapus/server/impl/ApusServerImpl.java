package com.viomi.boot.xmapus.server.impl;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import com.viomi.boot.xmapus.common.CommRespCode;
import com.viomi.boot.xmapus.common.ResultResp;
import com.viomi.boot.xmapus.handler.ApusMethodHandler;
import com.viomi.boot.xmapus.handler.ApusServiceHandler;
import com.viomi.boot.xmapus.server.ApusServer;
import com.viomi.boot.xmapus.utils.ApusUtils;

@Component
public class ApusServerImpl implements ApusServer.Iface {

	@Override
	public String execute(String paramsJson) {
		ApusUtils.parseParamMap(paramsJson);
		String serviceSimpleName = ApusUtils.getServiceSimpleName();
		if (StringUtils.isBlank(serviceSimpleName)) {
			return "参数method不能为空";
		}

		String methodName = ApusUtils.getMethodName();
		ApusServiceHandler apusServiceHandler = new ApusServiceHandler();
		Object serviceInstance = apusServiceHandler.getServiceInstance(serviceSimpleName);
		ApusMethodHandler apusMethod = new ApusMethodHandler(serviceInstance, methodName, ApusUtils.getParams(),
				ApusUtils.getHeader());
		try {
			return apusMethod.invoke();
		} catch (NoSuchMethodException e) {
			return ResultResp.fail(CommRespCode.FAIL, "指定的服务不存在！methodName = " + methodName).toString();
		} catch (Exception e) {
			return ResultResp.fail(CommRespCode.FAIL, "调用服务出错！methodName = " + methodName).toString();
		}
	}
}
