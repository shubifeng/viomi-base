##使用说明
- 1.默认：引入当前jar包会自动扫描

启动时需指定productMode=2;disconfFlag=1环境变量

在引入需要导入的配置文件
@DisconfFile(filename = "jdbc-proxool.properties")

配置后项目启动时自动下载jdbc-proxool.properties


- 2.扩展自定义

复制disconf.xml 然后复制到 resource目录下 

使用导入@ImportResource({"classpath:disconf.xml"})



注意：被扫描的文件只能是com.viomi的路径，否则需要在springboot配置文件中指定扫描包参数
