package com.viomi.disconf;

import com.baidu.disconf.client.DisconfMgrBeanSecond;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.core.env.Environment;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * @author：balthie@126.com
 * @createtime ： 2018年1月26日 下午2:40:22
 * @description baidu disconf基础框架启动配置入口类，使用@DisconfFile注解读取配置的项目，启动时需要扫描此配置类，
 * ViomiDisconfMgrBean会按spring容器最高优先级加载，详见 ViomiDisconfMgrBean。
 * 使用了BeanDefinitionRegistryPostProcessor机制保证在spring初始化bean前加载 disconf
 * @since version V2.0
 */
@Order(10)
@Configuration
@Slf4j
public class DisconfSetupConfig implements EnvironmentAware {

    // 是否使用disconf基础框架 ：使用
    public static final String DISCONFFLAG_ON = "1";
    // disconf 环境参数：开发环境
    public static final String DISCONFENV_DEV = "rd";
    // disconf 环境参数：测试环境
    public static final String DISCONFENV_TEST = "qa";
    // disconf 环境参数：生产环境
    public static final String DISCONFENV_ONLINE = "online";

    // disconf 环境参数：本地环境
    public static final String DISCONFENV_LOCAL = "local";

    private Environment env;

    @Override
    public void setEnvironment(Environment environment) {
        this.env = environment;
    }

    @Bean(destroyMethod = "destroy")
    public ViomiDisconfMgrBean ViomiDisconfMgrBean() {
        log.info("-------------------new ViomiDisconfMgrBean---------------");
        String f = "0";
        String[] active = env.getActiveProfiles();
        String m = "null";
        //指定springboot启动环境时
        if (Objects.nonNull(active) && active.length > 0) {
            List ss = Arrays.asList(active);
            if (ss.contains("local")) {
                m = "local";
            } else if (ss.contains("dev")) {
                m = "dev";
            } else if (ss.contains("test")) {
                m = "test";
            } else if (ss.contains("prod")) {
                m = "prod";
            }
            log.info("*********************springboot active启动配置环境变量runEnv为: {}", m);
        } else {
            if (env.containsProperty("runEnv")) {
                m = env.getProperty("runEnv");
                log.info("*********************配置环境变量runEnv为: {}", m);
            } else {
                log.info("*********************没有配置环境变量runEnv，默认为{}", m);
            }
        }
        SystemSetupConfig.productMode = m;

        if (env.containsProperty("disconfFlag")) {
            f = env.getProperty("disconfFlag");
            log.info("*********************配置环境变量disconfFlag为: {}", f);
        } else {
            log.info("*********************没有配置环境变量disconfFlag，默认为{}", f);
        }
        SystemSetupConfig.disconfFlag = f;


        ViomiDisconfMgrBean ds = new ViomiDisconfMgrBean();

        String s = "com.viomi";
        if (env.containsProperty("disconfScanPackage")) {
            s = env.getProperty("disconfScanPackage");
            log.info("*********************配置环境变量disconfScanPackage为: {}", s);
        } else {
            log.info("*********************没有配置环境变量disconfScanPackage，默认为{}", s);
        }

        ds.setScanPackage(s);

        log.info("---------------new ViomiDisconfMgrBean {} ---------------", ds.getOrder());
        return ds;
    }

    @Bean(initMethod = "init", destroyMethod = "destroy")
    public DisconfMgrBeanSecond DisconfMgrBeanSecond() {
        log.info("----------new DisconfMgrBeanSecond---------------");
        DisconfMgrBeanSecond dmb = new DisconfMgrBeanSecond();
        return dmb;
    }
}
