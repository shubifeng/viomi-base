package com.viomi.disconf;

import com.baidu.disconf.client.DisconfMgrBean;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;

import java.util.Objects;

/**
 * @author：balthie@126.com
 * @createtime ： 2018年1月26日 下午2:35:39
 * @description 根据服务容器环境变量，初始化disconf启动参数，实现根据运行环境加载不同启动参数参数
 * @since version V2.0
 */
@Slf4j
public class ViomiDisconfMgrBean extends DisconfMgrBean {


    public boolean initDisconf() {
        String disconfFlag = SystemSetupConfig.disconfFlag;
        log.debug("加载disconf配置中心标志[{}]", disconfFlag);
        if (!DisconfSetupConfig.DISCONFFLAG_ON.equals(disconfFlag)) {
            System.setProperty("disconf.enable.remote.conf", "false");
            log.debug("不进行disconf");
            return false;
        } else {
            System.setProperty("disconf.enable.remote.conf", "true");
        }
        String systemRunMode = SystemSetupConfig.productMode;
        if (Objects.isNull(systemRunMode)) {
            log.debug("运行模式为空，不执行disconf加载");
            return false;
        }

        String disconfEnv = null;
        if (Objects.equals(SystemRunMode.DEV.getCode(), systemRunMode)) {
            disconfEnv = DisconfSetupConfig.DISCONFENV_DEV;
        } else if (Objects.equals(SystemRunMode.TEST.getCode(), systemRunMode)) {
            disconfEnv = DisconfSetupConfig.DISCONFENV_TEST;
        } else if (Objects.equals(SystemRunMode.PROD.getCode(), systemRunMode)) {
            disconfEnv = DisconfSetupConfig.DISCONFENV_ONLINE;
        } else if (Objects.equals(SystemRunMode.LOCAL.getCode(), systemRunMode)) {
            disconfEnv = DisconfSetupConfig.DISCONFENV_LOCAL;
        } else {
            throw new RuntimeException("加载disconf配置中心错误：" + String.format("无法识别的运行模式[%s]", systemRunMode));
        }

        System.setProperty("disconf.env", disconfEnv);
        log.info("disconfFlag={},disconfEnv={},运行模式runEnv={}", disconfFlag, disconfEnv, systemRunMode);
        return true;
    }

    @Override
    public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry) throws BeansException {
        boolean bool = initDisconf();
        if (bool) {
            super.postProcessBeanDefinitionRegistry(registry);
        }
    }

    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
        super.postProcessBeanFactory(beanFactory);
    }
}
