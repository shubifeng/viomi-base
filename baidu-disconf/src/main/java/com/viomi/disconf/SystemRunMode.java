package com.viomi.disconf;


public enum SystemRunMode {
    LOCAL("local"), DEV("dev"), PROD("prod"), TEST("test");

    private String code;

    SystemRunMode(String code) {
        this.code = code;
    }

    public String getCode() {
        return this.code;
    }
}