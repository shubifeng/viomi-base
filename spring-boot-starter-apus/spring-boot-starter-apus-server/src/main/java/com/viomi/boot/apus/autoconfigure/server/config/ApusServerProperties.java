package com.viomi.boot.apus.autoconfigure.server.config;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 服务端配置文件
 *
 * @author ShuBifeng
 * @date 2018-01-04
 */
@Data
@NoArgsConstructor
@ConfigurationProperties(prefix = "apus.server")
public class ApusServerProperties {

    /**
     * 服务使用的端口
     */
    private int port;

    /**
     * 线程池数
     */
    private int threadPoolSize;


    /**
     * 服务名
     */
    private String serviceName;

}
