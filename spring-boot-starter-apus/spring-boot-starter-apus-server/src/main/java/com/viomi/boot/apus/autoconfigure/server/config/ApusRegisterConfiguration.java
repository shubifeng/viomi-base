package com.viomi.boot.apus.autoconfigure.server.config;


import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.regex.Pattern;

/**
 * @author ShuBifeng
 * @date 2018-01-04
 */
@Configuration
/*
@Import(EtcdAutoConfiguration.class)
*/
@AutoConfigureAfter({ApusAutoConfiguration.class})
@EnableConfigurationProperties({ApusServerProperties.class})
@Slf4j
public class ApusRegisterConfiguration {

    private final Pattern DEFAULT_PACKAGE_PATTERN = Pattern.compile(
            "(\\p{javaJavaIdentifierStart}\\p{javaJavaIdentifierPart}*\\.)*\\p{javaJavaIdentifierStart}\\p{javaJavaIdentifierPart}*");

  /*@Bean
  @ConditionalOnMissingBean
  @ConditionalOnProperty(value = "thrift.server.port", matchIfMissing = false)
  public AppRes etcdRegister(EtcdClient etcdClient,
      ApusServerProperties thriftServerProperties) {
    EtcdRegister register = new EtcdRegister();
    String serviceName = thriftServerProperties.getServiceName();

    int lastComma = serviceName.lastIndexOf(".");
    String interfaceName = serviceName.substring(0, lastComma);
    if (!DEFAULT_PACKAGE_PATTERN.matcher(interfaceName).matches()) {
      throw new ThriftServerException("interface name is not match to package pattern");
    }

    register.setPath("/dragon/service/" + interfaceName);

    String ip = InetAddressUtil.getLocalHostLANAddress().getHostAddress();

    String address = ip + ":" + String.valueOf(thriftServerProperties.getPort());
    register.setKey(address);
    register.setValue(address);

    register.setClient(etcdClient);
    register.setStart(true);

    String path = register.getPath() + "/" + register.getKey();
    log.info("path is {} register success!", path);
    return register;
  }*/

}
