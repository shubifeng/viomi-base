package com.viomi.boot.apus.autoconfigure.server.config;


import com.viomi.boot.apus.autoconfigure.server.annotation.ApusService;
import com.viomi.boot.apus.autoconfigure.server.exception.ApusServerException;
import com.xiaomi.apus.server.ServerConfig;
import lombok.extern.slf4j.Slf4j;
import org.apache.thrift.TProcessor;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.ClassUtils;

/**
 *
 */
@Configuration
@EnableConfigurationProperties({ApusServerProperties.class})
@Slf4j
public class ApusAutoConfiguration implements ApplicationContextAware {

    @Autowired
    private ApusServerProperties apusServerProperties;

    private ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }


    @Bean
    @ConditionalOnMissingBean
    @ConditionalOnProperty(value = "thrift.server.port", matchIfMissing = false)
    public ServerConfig thriftServer() {
        String[] beanNames = applicationContext.getBeanNamesForAnnotation(ApusService.class);
        if (beanNames == null || beanNames.length == 0) {
            log.error("apusService bean name is null or empty");
            throw new ApusServerException("no apus com.viomi.base.mysql.tkmybatis.service");
        }

        if (beanNames != null) {
            Object bean = applicationContext.getBean(beanNames[0]);

            Class<?> serviceClass;

            Class<TProcessor> processorClass = null;
            Class<?> ifaceClass = null;

            Class<?>[] handlerInterfaces = ClassUtils.getAllInterfaces(bean);

            for (Class<?> interfaceClass : handlerInterfaces) {
                if (!interfaceClass.getName().endsWith("$Iface")) {
                    continue;
                }

                serviceClass = interfaceClass.getDeclaringClass();
                if (serviceClass == null) {
                    continue;
                }

                for (Class<?> innerClass : serviceClass.getDeclaredClasses()) {
                    if (!innerClass.getName().endsWith("$Processor")) {
                        continue;
                    }

                    if (!TProcessor.class.isAssignableFrom(innerClass)) {
                        continue;
                    }

                    if (ifaceClass != null) {
                        throw new IllegalStateException("Multiple Thrift Ifaces defined on handler");
                    }

                    ifaceClass = interfaceClass;
                    processorClass = (Class<TProcessor>) innerClass;
                    break;
                }
            }


            if (ifaceClass == null) {
                log.error("iface class is null");
                throw new IllegalStateException("No Thrift Ifaces found on handler");
            }

     /* ServerConfig config = new ServerConfig.Builder(ifaceClass)
              .registryFactory(new ServiceCenterRegistryFactory(ifaceClasss))
              .metricsFactory(new OpenFalconMetricsFactory("method", '/'))
              .port(12347).build();
      Bootstrap bootstrap = new Bootstrap<>(config, ifaceClass.newInstance());

      Constructor<TProcessor> processorConstructor = processorClass.getConstructor(ifaceClass);
      TProcessor processor = BeanUtils.instantiateClass(processorConstructor, bean);

      thriftServerProperties.setServiceName(ifaceClass.getCanonicalName());
*/

            log.info("thrift server is starting, com.viomi.base.mysql.tkmybatis.service name is {}, port is {}",
                    apusServerProperties.getServiceName(), apusServerProperties.getPort());
        }


        return null;
    }

}