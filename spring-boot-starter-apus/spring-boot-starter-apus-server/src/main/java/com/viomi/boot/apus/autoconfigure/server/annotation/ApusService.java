package com.viomi.boot.apus.autoconfigure.server.annotation;

import org.springframework.stereotype.Component;

import java.lang.annotation.*;

/**
 * apus的服务实现类注解
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@Component
public @interface ApusService {
}
