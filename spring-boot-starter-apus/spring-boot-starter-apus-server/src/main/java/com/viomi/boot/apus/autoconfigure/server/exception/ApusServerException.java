package com.viomi.boot.apus.autoconfigure.server.exception;

/**
 * @author ShuBifeng
 * @date 2018-01-04
 */
public class ApusServerException extends RuntimeException {

    public ApusServerException(String message) {
        super(message);
    }

    public ApusServerException(String message, Throwable t) {
        super(message, t);
    }

}
