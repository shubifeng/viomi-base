package com.viomi.boot.tkmybatis.mapper;

import com.viomi.base.model.entity.tkmybatis.BaseEntity;
import tk.mybatis.mapper.common.IdsMapper;
import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;
import tk.mybatis.mapper.common.special.InsertListMapper;

import java.io.Serializable;

/**
 * 功能简述:数据库操作行为扩展接口<br>
 * 详细描述:基础的数据访问接口, 默认实现增删改查<br>
 *
 * @param <K> Key, 数据库实体类的主键类型，也需要实现Serializable, Comparable <br>
 * @param <E> Entity, 数据库实体类的自身类型<br>
 * @author Shubifeng
 */
public interface BaseMapper<E extends BaseEntity<K, E>, K extends Serializable & Comparable<K>> extends IdsMapper<E>, Mapper<E>, MySqlMapper<E>, InsertListMapper<E> {
}
