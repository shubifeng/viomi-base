package com.viomi.boot.tkmybatis.dao;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.viomi.base.model.entity.tkmybatis.BaseEntity;
import com.viomi.boot.tkmybatis.mapper.BaseMapper;
import com.viomi.boot.tkmybatis.support.Filter;
import com.viomi.boot.tkmybatis.support.Order;
import com.viomi.boot.tkmybatis.support.Pageable;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ResolvableType;
import tk.mybatis.mapper.MapperException;
import tk.mybatis.mapper.entity.EntityColumn;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;
import tk.mybatis.mapper.mapperhelper.EntityHelper;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.*;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * 功能简述:基础数据访问服务接口的实现<br>
 * 详细描述:<br>
 * [1] 属性baseMapper是通过范型依赖注入<br>
 *
 * @param <ID> Key, 数据库实体类的主键类型，也需要实现Serializable, Comparable <br>
 * @param <E>  Entity, 数据库实体类的自身类型<br>
 * @author Shubifeng
 */
@Slf4j
public class BaseDaoImpl<E extends BaseEntity<ID, E>, ID extends Serializable & Comparable<ID>> implements BaseDao<E, ID> {

    private final static String COMMA_SEPARATOR = ",";
    private static final String NULL_TIPS = "{}值不能为null";
    private static final String NULL_TIPS2 = "%s值不能为null";
    private static final String EMPTY_EX = " 1 = 2 ";
    protected Class<E> entityClass;
    /**
     * 范型依赖注入，注入原则如下（假如E=Member，K=Long）<br>
     * [1] 根据范型输入，确定baseMapper的类型定为BaseMapper<Member,Long> <br>
     * [2] Spring寻找所有已经实现BaseMapper<Member,Long>的实例，如果有且只有一个实例，就会自动注入，否则会报错<br>
     */
    @Autowired
    private BaseMapper<E, ID> baseMapper;

    /**
     * 默认构造函数
     */
    @SuppressWarnings("unchecked")
    public BaseDaoImpl() {
        ResolvableType resolvableType = ResolvableType.forClass(getClass());
        entityClass = (Class<E>) resolvableType.getSuperType().getGeneric().resolve();
    }

    /**
     * 重载方法
     *
     * @param entity
     * @return
     */
    @Override
    public ID save(E entity) {
        if (entity.getId() == null) {
            create(entity);
        } else {
            updateNotNull(entity);
        }
        return entity.getId();
    }

    /**
     * 重载方法
     *
     * @param entities
     */
    @Override
    public void save(List<E> entities) {
        entities.forEach(e -> save(e));
    }

    @Override
    public void saveList(List<E> newList, List<E> oldList) {
        this.saveList(newList, oldList, BaseEntity::getId);
    }

    @Override
    public void saveList(List<E> newList, List<E> oldList, Function<E, Object> getKey) {
        this.saveList(newList, oldList, getKey, null);
    }

    @Override
    public void saveList(List<E> newList, List<E> oldList, Function<E, Object> getKey, BinaryOperator<E> assembler) {
        this.saveList(newList, oldList, getKey, assembler, null, null, null);
    }

    @Override
    public void saveList(List<E> newList, List<E> oldList, Function<E, Object> getKey, BinaryOperator<E> assembler, Function<List<E>, Boolean> deleter, Function<List<E>, Boolean> creater, Function<List<E>, Boolean> updater) {
        //New List Map
        Map<Object, E> newMap = Maps.newHashMap();
        newList.stream().forEach(e -> newMap.put(getKey.apply(e), e));
        //Old List Map
        Map<Object, E> oldMap = Maps.newHashMap();
        oldList.stream().forEach(e -> oldMap.put(getKey.apply(e), e));
        //提取 createList
        List<E> createList = newList.stream().filter(e -> oldMap.get(getKey.apply(e)) == null).collect(Collectors.toList());
        //提取 deleteList
        List<E> deleteList = oldList.stream().filter(e -> newMap.get(getKey.apply(e)) == null).collect(Collectors.toList());
        //提取 updateList
        List<E> updateList = oldList.stream().filter(e -> newMap.get(getKey.apply(e)) != null).collect(Collectors.toList());
        //指定更新内容
        if (assembler != null) {
            updateList = updateList.stream().map(e -> assembler.apply(e, newMap.get(getKey.apply(e)))).collect(Collectors.toList());
        }
        //执行删除函数
        if (CollectionUtils.isNotEmpty(deleteList)) {
            List<ID> deleteIds = deleteList.stream().map(e -> e.getId()).collect(Collectors.toList());
            this.delete(deleteIds);
            if (deleter != null) {
                deleter.apply(deleteList);
            }
        }
        //执行新增函数
        if (CollectionUtils.isNotEmpty(createList)) {
            this.create(createList);
            if (creater != null) {
                creater.apply(createList);
            }
        }
        //执行更新函数
        if (CollectionUtils.isNotEmpty(updateList)) {
            this.update(updateList);
            if (updater != null) {
                updater.apply(updateList);
            }
        }
    }

    /**
     * 重载方法
     *
     * @param entity
     * @return
     */
    @Override
    public ID create(E entity) {
        this.baseMapper.insertSelective(entity);
        return entity.getId();
    }

    /**
     * 重载方法
     *
     * @param entities
     * @return
     */
    @Override
    public void create(List<E> entities) {
        this.baseMapper.insertList(entities);
    }

    /**
     * 重载方法
     *
     * @param entities
     * @return
     */
    @Override
    public List<ID> create2(List<E> entities) {
        List<ID> ids = Lists.newArrayList();
        entities.forEach(e -> {
            create(e);
            ids.add(e.getId());
        });
        return ids;
    }

    /**
     * 功能简述:是否存在<br>
     * 详细描述:根据主键判断是否存在<br>
     *
     * @param id
     * @return
     */
    @Override
    public boolean exists(ID id) {
        return this.baseMapper.existsWithPrimaryKey(id);
    }

    /**
     * 重载方法
     *
     * @param id
     * @return
     */
    @Override
    public int delete(ID id) {
        return this.baseMapper.deleteByPrimaryKey(id);
    }

    /**
     * 重载方法
     *
     * @param ids
     * @return
     */
    @Override
    public int delete(Iterable<ID> ids) {
        if (Iterables.isEmpty(ids)) {
            return 0;
        }
        return this.baseMapper.deleteByIds(StringUtils.join(ids, COMMA_SEPARATOR));
    }

    /**
     * 重载方法
     *
     * @param entity
     * @return
     */
    @Override
    public int delete(E entity) {
        return this.baseMapper.delete(entity);
    }

    /**
     * 重载方法
     *
     * @param filters
     * @return
     */
    @Override
    public int deleteByFilter(List<Filter> filters) {
        return this.baseMapper.deleteByExample(toExample(filters, null));
    }

    /**
     * 重载方法
     *
     * @param entity
     * @return
     */
    @Override
    public int update(E entity) {
        return this.baseMapper.updateByPrimaryKey(entity);
    }

    /**
     * 重载方法
     *
     * @param entities
     * @return
     */
    @Override
    public int update(List<E> entities) {
        entities.forEach(e -> update(e));
        return 1;
    }

    /**
     * 重载方法
     *
     * @param entity
     * @param filters
     * @return
     */
    @Deprecated
    @Override
    public int updateByFilter(E entity, List<Filter> filters) {
        // TODO Auto-generated method stub
        return 0;
    }

    /**
     * 重载方法
     *
     * @param entity
     * @return
     */
    @Override
    public int updateNotNull(E entity) {
        return this.baseMapper.updateByPrimaryKeySelective(entity);
    }

    /**
     * 重载方法
     *
     * @param entities
     * @return
     */
    @Override
    public int updateNotNull(List<E> entities) {
        entities.forEach(e -> updateNotNull(e));
        return 1;
    }

    /**
     * 重载方法
     *
     * @param entity
     * @param filters
     * @return
     */
    @Override
    public int updateNotNullByFilter(E entity, List<Filter> filters) {
        return this.baseMapper.updateByExampleSelective(entity, toExample(filters, null));
    }

    /**
     * 重载方法
     *
     * @param id
     * @return
     */
    @Override
    public E find(ID id) {
        if (null == id) {
            return null;
        } else {
            return this.baseMapper.selectByPrimaryKey(id);
        }
    }

    /**
     * 重载方法
     *
     * @param entity
     * @return
     */
    @Override
    public List<E> find(E entity) {
        return this.baseMapper.select(entity);
    }

    /**
     * 重载方法
     *
     * @param entity
     * @param orders
     * @return
     */
    @Override
    public List<E> find(E entity, List<Order> orders) {
        return this.baseMapper.selectByExample(toExample(entity, orders));
    }

    /**
     * 重载方法
     *
     * @param ids
     * @return
     */
    @Override
    public List<E> find(Iterable<ID> ids) {
        if (Iterables.isEmpty(ids)) {
            return null;
        } else {
            return this.baseMapper.selectByIds(StringUtils.join(ids, COMMA_SEPARATOR));
        }
    }

    /**
     * 重载方法
     *
     * @return
     */
    @Override
    public List<E> findAll() {
        return this.baseMapper.selectAll();
    }

    /**
     * 功能简述:<br>
     * 详细描述:<br>
     *
     * @param entity
     * @return
     */
    @Override
    public E findOne(E entity) {
        return this.baseMapper.selectOne(entity);
    }

    /**
     * 功能简述:<br>
     * 详细描述:<br>
     *
     * @param pageable
     * @return
     */
    public List<E> find(Pageable pageable) {
        return this.baseMapper.selectByExample(toExample(pageable));
    }

    /**
     * 功能简述:<br>
     * 详细描述:<br>
     *
     * @param filters
     * @param orders
     * @return
     */
    @Override
    public List<E> find(List<Filter> filters, List<Order> orders) {
        return this.baseMapper.selectByExample(toExample(filters, orders));
    }

    /**
     * 重载方法
     *
     * @param filters
     * @return
     */
    @Override
    public List<E> findByFilter(List<Filter> filters) {
        return this.baseMapper.selectByExample(toExample(filters, null));
    }

    /**
     * 重载方法
     *
     * @return
     */
    @Override
    public long count() {
        return this.baseMapper.selectCount(null);
    }

    /**
     * 重载方法
     *
     * @param entity
     * @return
     */
    @Override
    public long count(E entity) {
        return this.baseMapper.selectCount(entity);
    }

    /**
     * 重载方法
     *
     * @param filters
     * @return
     */
    @Override
    public long countByFilter(List<Filter> filters) {
        return this.baseMapper.selectCountByExample(toExample(filters, null));
    }

    /**
     * 重载方法
     *
     * @param pageable
     * @return
     */
    @Override
    public List<E> page(Pageable pageable) {
        if (pageable.isCount() == null) {
            PageHelper.startPage(pageable.getPageNum(), pageable.getPageSize());
        } else {
            PageHelper.startPage(pageable.getPageNum(), pageable.getPageSize(), pageable.isCount());
        }
        return find(pageable);
    }

    /**
     * 功能简述:实体分页查询<br>
     * 详细描述:根据pageable条件查询实体<br>
     *
     * @param pageable
     * @return
     */
    @Override
    public PageInfo<E> page2(Pageable pageable) {
        return new PageInfo<E>(page(pageable));
    }

    /**
     * 功能简述:实体分页查询<br>
     * 详细描述:根据分页条件,过滤规则,排序规则查询实体<br>
     *
     * @param pageNum  为0时不分页,查所有
     * @param pageSize
     * @param filters
     * @param orders
     * @return
     */
    @Override
    public List<E> page(Integer pageNum, Integer pageSize, List<Filter> filters, List<Order> orders) {
        PageHelper.startPage(pageNum, pageSize);
        if (pageNum <= 0) {
            PageHelper.clearPage();
        }
        return this.baseMapper.selectByExample(toExample(filters, orders));
    }

    /**
     * 功能简述:实体分页查询<br>
     * 详细描述:根据分页条件,过滤规则,排序规则查询实体<br>
     *
     * @param pageNum  为0时不分页,查所有
     * @param pageSize
     * @param filters
     * @param orders
     * @return
     */
    @Override
    public PageInfo<E> page2(Integer pageNum, Integer pageSize, List<Filter> filters, List<Order> orders) {
        return new PageInfo<E>(page(pageNum, pageSize, filters, orders));
    }

    /**
     * 功能简述:<br>
     * 详细描述:<br>
     *
     * @param filters
     * @param orders
     * @return
     */
    protected Example toExample(List<Filter> filters, List<Order> orders) {
        Example example = new Example(entityClass);
        if (filters != null) {
            Criteria criteria = example.createCriteria();
            for (Filter filter : filters) {
                initCriteria(criteria, filter);
            }
        }
        if (orders != null) {
            for (Order order : orders) {
                switch (order.getDirection()) {
                    case asc:
                        example.orderBy(order.getProperty()).asc();
                        break;
                    case desc:
                        example.orderBy(order.getProperty()).desc();
                        break;
                    default:
                        break;
                }
            }
        }
        return example;
    }

    protected Example toExample(Pageable pageable) {
        Example example = new Example(entityClass);
        List<Filter> filters = pageable.getFilters();
        List<Order> orders = pageable.getOrders();
        Criteria criteria = example.createCriteria();
        if (filters != null) {
            for (Filter filter : filters) {
                initCriteria(criteria, filter);
            }
        }
        if (pageable.getOrderProperty() != null) {
            Order.Direction orderDirection = pageable.getOrderDirection();
            if (Objects.equals(orderDirection, Order.Direction.asc)) {
                example.orderBy(pageable.getOrderProperty()).asc();
            } else {
                example.orderBy(pageable.getOrderProperty()).desc();
            }
        }

        String searchProperty = pageable.getSearchProperty();
        String searchValue = pageable.getSearchValue();
        if ((StringUtils.isNotBlank(searchProperty)) && (StringUtils.isNotBlank(searchValue))) {
            criteria.andLike(searchProperty, "%" + searchValue + "%");
        }
        String searchCycleProperty = pageable.getSearchCycleProperty();
        Integer searchCycleValue = pageable.getSearchCycleValue();
        //TODO 循环周期查询暂时不放开 by 舒笔锋
        /*if (searchCycleProperty != null && searchCycleValue != null) {
            criteria.andGreaterThanOrEqualTo(searchCycleProperty, DateUtils.getDateByType(new Date(), searchCycleValue));
        }*/
        //        example.orderBy(pageable.getOrderProperty()).asc();
        if (orders != null) {
            for (Order order : orders) {
                switch (order.getDirection()) {
                    case asc:
                        example.orderBy(order.getProperty()).asc();
                        break;
                    case desc:
                        example.orderBy(order.getProperty()).desc();
                        break;
                    default:
                        break;
                }
            }
        }
        return example;
    }

    @SuppressWarnings("rawtypes")
    private void initCriteria(Criteria criteria, Filter filter) {
        String property = filter.getProperty();
        Filter.Operator operator = filter.getOperator();
        Object value = filter.getValue();
        if (value == null && !Objects.equals(Filter.Operator.isNull, operator) && !Objects.equals(Filter.Operator.isNotNull, operator)) {
            log.error(NULL_TIPS, property);
            throw new MapperException(String.format(NULL_TIPS2, property));
        }
        switch (operator) {
            case eq:
                criteria.andEqualTo(property, value);
                break;
            case ne:
                criteria.andNotEqualTo(property, value);
                break;
            case gt:
                criteria.andGreaterThan(property, value);
                break;
            case lt:
                criteria.andLessThan(property, value);
                break;
            case ge:
                criteria.andGreaterThanOrEqualTo(property, value);
                break;
            case le:
                criteria.andLessThanOrEqualTo(property, value);
                break;
            case like:
                criteria.andLike(property, "%" + value + "%");
                break;
            case in:
                Collection valueColletion1 = (Collection) value;
                if (!valueColletion1.isEmpty()) {
                    criteria.andIn(property, (Iterable) value);
                } else {
                    log.info("空集合in查询转eq");
                    //做替换,防止空集合时SQL执行报错,但建议还是在使用notIn或者in之前判断集合是否为空,以减少IO
                    criteria.andCondition(EMPTY_EX);
                }
                break;
            case notIn:
                Collection valueColletion2 = (Collection) value;
                if (!valueColletion2.isEmpty()) {
                    criteria.andNotIn(property, (Iterable) value);
                } else {
                    log.info("空集合notIn查询转eq");
                    //做替换,防止空集合时SQL执行报错,但建议还是在使用notIn或者in之前判断集合是否为空,以减少IO
                    criteria.andCondition(EMPTY_EX);
                }
                break;
            case isNull:
                criteria.andIsNull(property);
                break;
            case isNotNull:
                criteria.andIsNotNull(property);
                break;
            default:
                break;
        }
    }

    protected Example toExample(E record, List<Order> orders) {
        Example example = new Example(entityClass);
        if (record != null) {
            Criteria criteria = example.createCriteria();
            Set<EntityColumn> columnList = EntityHelper.getColumns(entityClass);
            for (EntityColumn column : columnList) {
                String value = null;
                try {
                    value = BeanUtils.getProperty(record, column.getProperty());
                } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
                    e.printStackTrace();
                }
                if (value != null) {
                    criteria.andEqualTo(column.getProperty(), value);
                }
            }
        }
        if (orders != null) {
            for (Order order : orders) {
                switch (order.getDirection()) {
                    case asc:
                        example.orderBy(order.getProperty()).asc();
                        break;
                    case desc:
                        example.orderBy(order.getProperty()).desc();
                        break;
                    default:
                        break;
                }
            }
        }
        return example;
    }

    /**
     * 功能简述:拼装成MAP参数供自定义sql使用<br>
     * 详细描述:<br>
     *
     * @param page
     * @return
     */
    protected Map<String, Object> toMap(Pageable page) {
        Map<String, Object> params = Maps.newHashMap();
        if (StringUtils.isNotBlank(page.getSearchProperty()) && StringUtils.isNotBlank(page.getSearchValue())) {
            params.put(page.getSearchProperty(), page.getSearchValue());
        }
        if (StringUtils.isNotBlank(page.getOrderProperty()) && page.getOrderDirection() != null) {
            params.put("orderBy", page.getOrderProperty() + page.getOrderDirection().getMsg());
        }
        List<Filter> filters = page.getFilters();
        if (CollectionUtils.isNotEmpty(filters)) {
            filters.stream().filter(f -> StringUtils.isNotBlank(f.getProperty()) && f.getValue() != null).forEach(f -> {
                String property = f.getProperty();
                Filter.Operator operator = f.getOperator();
                Object value = f.getValue();
                switch (operator) {
                    case gt:
                        params.put(property + "Start", value);
                        break;
                    case lt:
                        params.put(property + "End", value);
                        break;
                    case ge:
                        params.put(property + "Start", value);
                        break;
                    case le:
                        params.put(property + "End", value);
                        break;
                    case in:
                        params.put(property + "In", value);
                        break;
                    default:
                        params.put(property, value);
                }
            });
        }
        return params;
    }
}
