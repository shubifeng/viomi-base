package com.viomi.boot.tkmybatis.support;

import com.google.common.collect.ImmutableList;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author ShuBifeng
 * @date 2018-01-04
 */
public class Pageable implements Serializable {

    /**
     * 默认页码
     */
    public static final int DEFAULT_PAGE_NUMBER = 1;
    /**
     * 默认每页记录数
     */
    public static final int DEFAULT_PAGE_SIZE = 20;
    /**
     * 最大每页记录数
     */
    public static final int MAX_PAGE_SIZE = 1000;
    private static final long serialVersionUID = -3930180379790344299L;
    /**
     * 页码
     */
    private int pageNum = DEFAULT_PAGE_NUMBER;

    /**
     * 每页记录数
     */
    private int pageSize = DEFAULT_PAGE_SIZE;

    /**
     * 搜索属性
     */
    private String searchProperty;

    /**
     * 搜索值
     */
    private String searchValue;

    /**
     * 查询周期的属性
     */
    private String searchCycleProperty;

    /**
     * 查询周期：0 最近一周； 1 最近一个月 ；2 最近三个月 ；3 最近一年
     */
    private Integer searchCycleValue;

    /**
     * 包含isCount查询
     */
    private Boolean isCount;

    /**
     * 排序属性
     */
    private String orderProperty;

    /**
     * 排序方向
     */
    private Order.Direction orderDirection;

    /**
     * 筛选
     */
    private List<Filter> filters = new ArrayList<Filter>();

    /**
     * 排序
     */
    private List<Order> orders = new ArrayList<Order>();

    /**
     * 构造方法
     */
    public Pageable() {
    }

    /**
     * 构造方法
     *
     * @param pageNum  页码
     * @param pageSize 每页记录数
     */
    public Pageable(Integer pageNum, Integer pageSize) {
        if (pageNum != null && pageNum >= 1) {
            this.pageNum = pageNum;
        }
        if (pageSize != null && pageSize >= 1 && pageSize <= MAX_PAGE_SIZE) {
            this.pageSize = pageSize;
        }
    }

    public List<Order> buildOrderList() {
        List<Order> orderList = null;
        if (StringUtils.isNotBlank(this.getOrderProperty()) && this.getOrderDirection() != null) {
            if (Order.Direction.desc.equals(this.getOrderDirection())) {
                orderList = ImmutableList.of(Order.desc(this.getOrderProperty()));
            }
            orderList = ImmutableList.of(Order.asc(this.getOrderProperty()));
        }
        return orderList;
    }

    /**
     * 获取页码
     *
     * @return 页码
     */
    public int getPageNum() {
        return pageNum;
    }

    /**
     * 设置页码
     *
     * @param pageNum 页码
     */
    public void setPageNum(int pageNum) {
        if (pageNum < 1) {
            pageNum = DEFAULT_PAGE_NUMBER;
        }
        this.pageNum = pageNum;
    }

    /**
     * 获取每页记录数
     *
     * @return 每页记录数
     */
    public int getPageSize() {
        return pageSize;
    }

    /**
     * 设置每页记录数
     *
     * @param pageSize 每页记录数
     */
    public void setPageSize(int pageSize) {
        if (pageSize < 1 || pageSize > MAX_PAGE_SIZE) {
            pageSize = DEFAULT_PAGE_SIZE;
        }
        this.pageSize = pageSize;
    }

    /**
     * 获取搜索属性
     *
     * @return 搜索属性
     */
    public String getSearchProperty() {
        return searchProperty;
    }

    /**
     * 设置搜索属性
     *
     * @param searchProperty 搜索属性
     */
    public void setSearchProperty(String searchProperty) {
        this.searchProperty = searchProperty;
    }

    /**
     * 获取 count
     *
     * @return 返回 count
     */
    public Boolean isCount() {
        return isCount;
    }

    /**
     * 设置 对isCount进行赋值
     *
     * @param isCount
     */
    public void setIsCount(Boolean isCount) {
        this.isCount = isCount;
    }

    /**
     * 获取搜索值
     *
     * @return 搜索值
     */
    public String getSearchValue() {
        return searchValue;
    }

    /**
     * 设置搜索值
     *
     * @param searchValue 搜索值
     */
    public void setSearchValue(String searchValue) {
        this.searchValue = searchValue;
    }

    /**
     * 获取排序属性
     *
     * @return 排序属性
     */
    public String getOrderProperty() {
        return orderProperty;
    }

    /**
     * 设置排序属性
     *
     * @param orderProperty 排序属性
     */
    public void setOrderProperty(String orderProperty) {
        this.orderProperty = orderProperty;
    }

    /**
     * 获取排序方向
     *
     * @return 排序方向
     */
    public Order.Direction getOrderDirection() {
        return orderDirection;
    }

    /**
     * 设置排序方向
     *
     * @param orderDirection 排序方向
     */
    public void setOrderDirection(Order.Direction orderDirection) {
        this.orderDirection = orderDirection;
    }

    /**
     * 获取筛选
     *
     * @return 筛选
     */
    public List<Filter> getFilters() {
        return filters;
    }

    /**
     * 设置筛选
     *
     * @param filters 筛选
     */
    public void setFilters(List<Filter> filters) {
        this.filters = filters;
    }

    /**
     * 获取排序
     *
     * @return 排序
     */
    public List<Order> getOrders() {
        return orders;
    }

    /**
     * 设置排序
     *
     * @param orders 排序
     */
    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

    public String getSearchCycleProperty() {
        return searchCycleProperty;
    }

    public void setSearchCycleProperty(String searchCycleProperty) {
        this.searchCycleProperty = searchCycleProperty;
    }

    public Integer getSearchCycleValue() {
        return searchCycleValue;
    }

    public void setSearchCycleValue(Integer searchCycleValue) {
        this.searchCycleValue = searchCycleValue;
    }

    /**
     * 重写equals方法
     *
     * @param obj 对象
     * @return 是否相等
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        Pageable other = (Pageable) obj;
        return new EqualsBuilder().append(getPageNum(), other.getPageNum())
                .append(getPageSize(), other.getPageSize())
                .append(getSearchProperty(), other.getSearchProperty())
                .append(getSearchValue(), other.getSearchValue())
                .append(getOrderProperty(), other.getOrderProperty())
                .append(getOrderDirection(), other.getOrderDirection())
                .append(getFilters(), other.getFilters())
                .append(getOrders(), other.getOrders())
                .isEquals();
    }

    /**
     * 重写hashCode方法
     *
     * @return HashCode
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(getPageNum())
                .append(getPageSize())
                .append(getSearchProperty())
                .append(getSearchValue())
                .append(getOrderProperty())
                .append(getOrderDirection())
                .append(getFilters())
                .append(getOrders())
                .toHashCode();
    }
}