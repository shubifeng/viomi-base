package com.viomi.boot.tkmybatis.support;

import com.google.common.collect.ImmutableList;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;
import java.util.List;

/**
 * 排序
 *
 * @author SHOP++ Team
 * @version 4.0
 */
public class Order implements Serializable {

    private static final long serialVersionUID = -3078342809727773232L;
    /**
     * 默认方向
     */
    private static final Direction DEFAULT_DIRECTION = Direction.desc;
    /**
     * 属性
     */
    private String property;
    /**
     * 方向
     */
    private Direction direction = DEFAULT_DIRECTION;

    /**
     * 构造方法
     */
    public Order() {
    }

    /**
     * 构造方法
     *
     * @param property  属性
     * @param direction 方向
     */
    public Order(String property, Direction direction) {
        this.property = property;
        this.direction = direction;
    }

    /**
     * 返回递增排序
     *
     * @param property 属性
     * @return 递增排序
     */
    public static Order asc(String property) {
        return new Order(property, Direction.asc);
    }

    /**
     * 返回递减排序
     *
     * @param property 属性
     * @return 递减排序
     */
    public static Order desc(String property) {
        return new Order(property, Direction.desc);
    }

    public static List<Order> buildOrderList(String orderBy, String direction) {
        List<Order> orderList = null;
        if (StringUtils.isNotBlank(orderBy) && StringUtils.isNotBlank(direction)) {
            if (Direction.desc.getMsg().equals(direction)) {
                orderList = ImmutableList.of(Order.desc(orderBy));
            }
            orderList = ImmutableList.of(Order.asc(orderBy));
        }
        return orderList;
    }

    /**
     * 获取属性
     *
     * @return 属性
     */
    public String getProperty() {
        return property;
    }

    /**
     * 设置属性
     *
     * @param property 属性
     */
    public void setProperty(String property) {
        this.property = property;
    }

    /**
     * 获取方向
     *
     * @return 方向
     */
    public Direction getDirection() {
        return direction;
    }

    /**
     * 设置方向
     *
     * @param direction 方向
     */
    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    /**
     * 重写equals方法
     *
     * @param obj 对象
     * @return 是否相等
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        Order other = (Order) obj;
        return new EqualsBuilder().append(getProperty(), other.getProperty()).append(getDirection(), other.getDirection()).isEquals();
    }

    /**
     * 重写hashCode方法
     *
     * @return HashCode
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(getProperty()).append(getDirection()).toHashCode();
    }

    /**
     * 方向
     */
    public enum Direction {
        /**
         * 递增
         */
        asc("asc"),
        /**
         * 递减
         */
        desc("desc");

        private String msg;

        Direction(String msg) {
            this.msg = msg;
        }

        public String getMsg() {
            return this.msg;
        }
    }
}