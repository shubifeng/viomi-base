package com.viomi.boot.tkmybatis.support;

import java.lang.reflect.Field;

/**
 * 功能简述:<br>
 *
 * @author ShuBifeng
 * @date 2018-01-04
 */
public class DalUtils {
    final static String SERIAL = "serialVersionUID";

    public static <T> boolean checkFieldsNull(T object) {
        boolean isAllNull = true;
        for (Field f : object.getClass().getDeclaredFields()) {
            f.setAccessible(true);
            try {
                if (f.getName().equals(SERIAL)) {
                    continue;
                } else {
                    if (f.get(object) != null) {
                        isAllNull = false;
                        break;
                    }
                }
            } catch (Exception e) {
            }
        }
        return isAllNull;
    }
}
