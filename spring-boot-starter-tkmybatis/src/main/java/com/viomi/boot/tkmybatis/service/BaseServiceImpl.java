package com.viomi.boot.tkmybatis.service;

import com.github.pagehelper.PageInfo;
import com.viomi.base.model.entity.tkmybatis.BaseEntity;
import com.viomi.boot.tkmybatis.dao.BaseDao;
import com.viomi.boot.tkmybatis.support.Filter;
import com.viomi.boot.tkmybatis.support.Order;
import com.viomi.boot.tkmybatis.support.Pageable;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.Serializable;
import java.util.List;
import java.util.function.BinaryOperator;
import java.util.function.Function;

/**
 * 功能简述:基础数据访问服务接口的实现<br>
 * 详细描述:<br>
 * [1] 属性baseDao是通过范型依赖注入<br>
 *
 * @param <ID> Key, 数据库实体类的主键类型，也需要实现Serializable, Comparable <br>
 * @param <E>  Entity, 数据库实体类的自身类型<br>
 * @author 2016年9月14日
 */
public class BaseServiceImpl<E extends BaseEntity<ID, E>, ID extends Serializable & Comparable<ID>> implements BaseService<E, ID> {

    /**
     * 范型依赖注入，注入原则如下（假如E=Member，K=Long）<br>
     * [1] 根据范型输入，确定baseDao的类型定为BaseDao<Member,Long> <br>
     * [2] Spring寻找所有已经实现BaseDao<Member,Long>的实例，如果有且只有一个实例，就会自动注入，否则会报错<br>
     */
    @Autowired
    private BaseDao<E, ID> baseDao;

    /**
     * 功能简述:<br>
     * 详细描述:<br>
     *
     * @param entity
     * @return
     */
    @Override
    public ID save(E entity) {
        return this.baseDao.save(entity);
    }

    @Override
    public void saveList(List<E> newList, List<E> oldList) {
        this.baseDao.saveList(newList, oldList);
    }

    @Override
    public void saveList(List<E> newList, List<E> oldList, Function<E, Object> getKey) {
        this.baseDao.saveList(newList, oldList, getKey);
    }

    @Override
    public void saveList(List<E> newList, List<E> oldList, Function<E, Object> getKey, BinaryOperator<E> assembler) {
        this.baseDao.saveList(newList, oldList, getKey, assembler);
    }

    @Override
    public void saveList(List<E> newList, List<E> oldList, Function<E, Object> getKey, BinaryOperator<E> assembler, Function<List<E>, Boolean> deleter, Function<List<E>, Boolean> creater, Function<List<E>, Boolean> updater) {
        this.baseDao.saveList(newList, oldList, getKey, assembler, deleter, creater, updater);
    }

    /**
     * 重载方法
     *
     * @param entity
     * @return:
     */
    @Override
    public ID create(E entity) {
        return this.baseDao.create(entity);
    }

    /**
     * 重载方法
     *
     * @param entities
     * @return
     */
    @Override
    public void create(List<E> entities) {
        this.baseDao.create(entities);
    }

    /**
     * 重载方法
     *
     * @param entities
     * @return
     */
    @Override
    public List<ID> create2(List<E> entities) {
        return this.baseDao.create2(entities);
    }

    /**
     * 功能简述:是否存在<br>
     * 详细描述:根据主键判断是否存在<br>
     *
     * @param id
     * @return
     */
    @Override
    public boolean exists(ID id) {
        return this.baseDao.exists(id);
    }

    /**
     * 重载方法
     *
     * @param id
     * @return
     */
    @Override
    public int delete(ID id) {
        return this.baseDao.delete(id);
    }

    /**
     * 重载方法
     *
     * @param ids
     * @return
     */
    @Override
    public int delete(Iterable<ID> ids) {
        return this.baseDao.delete(ids);
    }

    /**
     * 重载方法
     *
     * @param entity
     * @return
     */
    @Override
    public int delete(E entity) {
        return this.baseDao.delete(entity);
    }

    /**
     * 重载方法
     *
     * @param filters
     * @return
     */
    @Override
    public int deleteByFilter(List<Filter> filters) {
        return this.baseDao.deleteByFilter(filters);
    }

    /**
     * 重载方法
     *
     * @param entity
     * @return
     */
    @Override
    public int update(E entity) {
        return this.baseDao.update(entity);
    }

    /**
     * 重载方法
     *
     * @param entities
     * @return
     */
    @Override
    public int update(List<E> entities) {
        return this.baseDao.update(entities);
    }

    /**
     * 重载方法
     *
     * @param entity
     * @param filters
     * @return
     */
    @Deprecated
    @Override
    public int updateByFilter(E entity, List<Filter> filters) {
        return this.baseDao.updateByFilter(entity, filters);
    }

    /**
     * 重载方法
     *
     * @param entity
     * @return
     */
    @Override
    public int updateNotNull(E entity) {
        return this.baseDao.updateNotNull(entity);
    }

    /**
     * 重载方法
     *
     * @param entities
     * @return
     */
    @Override
    public int updateNotNull(List<E> entities) {
        return this.baseDao.updateNotNull(entities);
    }

    /**
     * 重载方法
     *
     * @param entity
     * @param filters
     * @return
     */
    @Override
    public int updateNotNullByFilter(E entity, List<Filter> filters) {
        return this.baseDao.updateNotNullByFilter(entity, filters);
    }

    /**
     * 重载方法
     *
     * @param id
     * @return
     */
    @Override
    public E find(ID id) {
        return this.baseDao.find(id);
    }

    /**
     * 重载方法
     *
     * @param entity
     * @return
     */
    @Override
    public List<E> find(E entity) {
        return this.baseDao.find(entity);
    }

    @Override
    public List<E> find(E entity, List<Order> orders) {
        return this.baseDao.find(entity, orders);
    }

    /**
     * 重载方法
     *
     * @param ids
     * @return
     */
    @Override
    public List<E> find(Iterable<ID> ids) {
        return this.baseDao.find(ids);
    }

    /**
     * 重载方法
     *
     * @return
     */
    @Override
    public List<E> findAll() {
        return this.baseDao.findAll();
    }

    /**
     * 功能简述:<br>
     * 详细描述:<br>
     *
     * @param entity
     * @return
     */
    @Override
    public E findOne(E entity) {
        return this.baseDao.findOne(entity);
    }

    /**
     * 重载方法
     *
     * @param filters
     * @param orders
     * @return
     */
    @Override
    public List<E> find(List<Filter> filters, List<Order> orders) {
        return this.baseDao.find(filters, orders);
    }

    /**
     * 重载方法
     *
     * @param filters
     * @return
     */
    @Override
    public List<E> findByFilter(List<Filter> filters) {
        return this.baseDao.findByFilter(filters);
    }

    /**
     * 重载方法
     *
     * @return
     */
    @Override
    public long count() {
        return this.baseDao.count();
    }

    /**
     * 重载方法
     *
     * @param entity
     * @return
     */
    @Override
    public long count(E entity) {
        return this.baseDao.count(entity);
    }

    /**
     * 重载方法
     *
     * @param filters
     * @return
     */
    @Override
    public long countByFilter(List<Filter> filters) {
        return this.baseDao.countByFilter(filters);
    }

    /**
     * 重载方法
     *
     * @param pageable
     * @return
     */
    @Override
    public List<E> page(Pageable pageable) {
        return this.baseDao.page(pageable);
    }

    /**
     * 功能简述:实体分页查询<br>
     * 详细描述:根据pageable条件查询实体<br>
     *
     * @param pageable
     * @return
     */
    @Override
    public PageInfo<E> page2(Pageable pageable) {
        return this.baseDao.page2(pageable);
    }

    /**
     * 重载方法
     *
     * @param first
     * @param count
     * @param filters
     * @param orders
     * @return
     */
    @Override
    public List<E> page(Integer first, Integer count, List<Filter> filters, List<Order> orders) {
        return this.baseDao.page(first, count, filters, orders);
    }

    /**
     * 功能简述:实体分页查询<br>
     * 详细描述:根据分页条件,过滤规则,排序规则查询实体<br>
     *
     * @param first
     * @param count
     * @param filters
     * @param orders
     * @return
     */
    @Override
    public PageInfo<E> page2(Integer first, Integer count, List<Filter> filters, List<Order> orders) {
        return this.baseDao.page2(first, count, filters, orders);
    }

}
