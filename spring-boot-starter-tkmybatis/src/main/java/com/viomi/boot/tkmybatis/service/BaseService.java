package com.viomi.boot.tkmybatis.service;

import com.github.pagehelper.PageInfo;
import com.viomi.base.model.entity.tkmybatis.BaseEntity;
import com.viomi.boot.tkmybatis.support.Filter;
import com.viomi.boot.tkmybatis.support.Order;
import com.viomi.boot.tkmybatis.support.Pageable;

import java.io.Serializable;
import java.util.List;
import java.util.function.BinaryOperator;
import java.util.function.Function;

/**
 * 功能简述: 基础数据访问服务接口<br>
 * 详细描述:<br>
 *
 * @param <ID> Key, 数据库实体类的主键类型，也需要实现Serializable, Comparable <br>
 * @param <E>  Entity, 数据库实体类的自身类型<br>
 * @author Shubifeng
 */
public interface BaseService<E extends BaseEntity<ID, E>, ID extends Serializable & Comparable<ID>> {

    /**
     * 功能简述:保存或更新<br>
     * 详细描述:<br>
     *
     * @param entity
     * @return
     */
    ID save(E entity);

    /**
     * 功能简述: 排序查询列表
     *
     * @param newList
     * @param oldList
     */
    void saveList(List<E> newList, List<E> oldList);

    void saveList(List<E> newList, List<E> oldList, Function<E, Object> getKey);

    void saveList(List<E> newList, List<E> oldList, Function<E, Object> getKey, BinaryOperator<E> assembler);

    void saveList(List<E> newList, List<E> oldList, Function<E, Object> getKey, BinaryOperator<E> assembler, Function<List<E>, Boolean> updater, Function<List<E>, Boolean> deleter, Function<List<E>, Boolean> creater);


    /**
     * 功能简述:保存实体<br>
     * 详细描述:保存entity全部字段包括null值<br>
     *
     * @param entity
     * @return
     */
    ID create(E entity);

    /**
     * 功能简述:保存多个实体<br>
     * 详细描述:当属性为null，保存数据库默认值<br>
     *
     * @param entities
     * @return
     */
    void create(List<E> entities);

    /**
     * 功能简述:保存多个实体<br>
     * 详细描述:当属性为null，保存数据库默认值<br>
     *
     * @param entities
     * @return
     */
    List<ID> create2(List<E> entities);

    /**
     * 功能简述:是否存在<br>
     * 详细描述:根据主键判断是否存在<br>
     *
     * @param id
     * @return
     */
    boolean exists(ID id);

    /**
     * 功能简述:删除实体<br>
     * 详细描述:根据ID删除实体<br>
     *
     * @param id
     * @return
     */
    int delete(ID id);

    /**
     * 功能简述:删除多个实体<br>
     * 详细描述:根据多个ID删除实体<br>
     *
     * @param ids
     * @return
     */
    int delete(Iterable<ID> ids);

    /**
     * 功能简述:删除多个实体<br>
     * 详细描述:根据entity值作为过滤删除实体<br>
     *
     * @param entity
     * @return
     */
    int delete(E entity);

    /**
     * 功能简述:删除多个实体<br>
     * 详细描述:根据filters作为过滤删除实体<br>
     *
     * @param filters
     * @return
     */
    int deleteByFilter(List<Filter> filters);

    /**
     * 功能简述:更新实体<br>
     * 详细描述:更新entity全部字段<br>
     *
     * @param entity
     * @return
     */
    int update(E entity);

    /**
     * 功能简述:更新多个实体<br>
     * 详细描述:更新entity全部字段<br>
     *
     * @param entities
     * @return
     */
    int update(List<E> entities);

    /**
     * 功能简述:更新多个实体<br>
     * 详细描述:更新entity全部字段<br>
     *
     * @param entity
     * @param filters
     * @return
     */
    @Deprecated
    int updateByFilter(E entity, List<Filter> filters);

    /**
     * 功能简述:更新实体<br>
     * 详细描述:更新entity非空字段值<br>
     *
     * @param entity
     * @return
     */
    int updateNotNull(E entity);

    /**
     * 功能简述:更新多个实体<br>
     * 详细描述:更新entity非空字段值<br>
     *
     * @param entities
     * @return
     */
    int updateNotNull(List<E> entities);

    /**
     * 功能简述:过滤条件更新多个实体<br>
     * 详细描述:更新entity非空字段值<br>
     *
     * @param entity
     * @param filters
     * @return
     */
    int updateNotNullByFilter(E entity, List<Filter> filters);

    /**
     * 功能简述:查找实体<br>
     * 详细描述:根据ID查找唯一实体<br>
     *
     * @param id
     * @return
     */
    E find(ID id);

    /**
     * 功能简述:查找多个实体<br>
     * 详细描述:根据entity字段过滤查找实体<br>
     *
     * @param entity
     * @return
     */
    List<E> find(E entity);

    /**
     * @param entity
     * @param orders
     * @return
     */
    List<E> find(E entity, List<Order> orders);

    /**
     * 功能简述:查找多个实体<br>
     * 详细描述:根据多个ID字段查找实体<br>
     *
     * @param ids
     * @return
     */
    List<E> find(Iterable<ID> ids);

    /**
     * 功能简述:查找全部实体<br>
     * 详细描述:查找全部实体<br>
     *
     * @return
     */
    List<E> findAll();


    /**
     * 功能简述:<br>
     * 详细描述:<br>
     *
     * @param entity
     * @return
     */
    E findOne(E entity);

    /**
     * 功能简述:查找多个实体<br>
     * 详细描述:根据filters过滤条件查找实体<br>
     *
     * @param filters
     * @return
     */
    List<E> findByFilter(List<Filter> filters);

    /**
     * 功能简述: 根据过滤条件与排序规则查找实体<br>
     * 详细描述:<br>
     *
     * @param filters
     * @param orders
     * @return
     */
    List<E> find(List<Filter> filters, List<Order> orders);

    /**
     * 功能简述:查询实体对象数量<br>
     * 详细描述:查询实体总数量<br>
     *
     * @return
     */
    long count();

    /**
     * 功能简述:查询实体对象数量<br>
     * 详细描述:根据entity属性过滤查询数量<br>
     *
     * @param entity
     * @return
     */
    long count(E entity);

    /**
     * 功能简述:查询实体对象数量<br>
     * 详细描述:根据filters过滤条件查询数量<br>
     *
     * @param filters
     * @return
     */
    long countByFilter(List<Filter> filters);

    /**
     * 功能简述:实体分页查询<br>
     * 详细描述:根据pageable条件查询实体<br>
     *
     * @param pageable
     * @return
     */
    List<E> page(Pageable pageable);

    /**
     * 功能简述:实体分页查询<br>
     * 详细描述:根据pageable条件查询实体<br>
     *
     * @param pageable
     * @return
     */
    PageInfo<E> page2(Pageable pageable);

    /**
     * 功能简述:实体分页查询<br>
     * 详细描述:根据分页条件,过滤规则,排序规则查询实体<br>
     *
     * @param first
     * @param count
     * @param filters
     * @param orders
     * @return
     */
    List<E> page(Integer first, Integer count, List<Filter> filters, List<Order> orders);

    /**
     * 功能简述:实体分页查询<br>
     * 详细描述:根据分页条件,过滤规则,排序规则查询实体<br>
     *
     * @param first
     * @param count
     * @param filters
     * @param orders
     * @return
     */
    PageInfo<E> page2(Integer first, Integer count, List<Filter> filters, List<Order> orders);
}
