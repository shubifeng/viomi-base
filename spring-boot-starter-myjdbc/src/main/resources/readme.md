默认采用若使用阿里数据连接池配置，详见
https://github.com/alibaba/druid/tree/master/druid-spring-boot-starter

其他连接池请自行排除依赖包



子项目需引入配置：
请根据自己的需要进行配置
---默认msyql
spring.datasource.url: jdbc:mysql://127.0.0.1:3306/member?serverTimezone=Asia/Shanghai&useUnicode=true&characterEncoding=utf8&zeroDateTimeBehavior=convertToNull
spring.datasource.username: root
spring.datasource.password: '0000'
spring.datasource.driverClassName: com.mysql.cj.jdbc.Driver
spring.datasource.type: com.alibaba.druid.pool.DruidDataSource
spring.datasource.name: my

---greenplum数据库
spring.datasource.druid.gp.url: jdbc:postgresql://192.168.1.211:2345/bizdata
spring.datasource.druid.gp.username: gpadmin
spring.datasource.druid.gp.password:
spring.datasource.druid.gp.driverClassName: org.postgresql.Driver
spring.datasource.druid.gp.type: com.alibaba.druid.pool.DruidDataSource
spring.datasource.druid.gp.name: druid



使用:

@Autowrite
private GpJdbcTemplate gpJdbcTemplate;
@Autowrite
private MyJdbcTemplate myJdbcTemplate;