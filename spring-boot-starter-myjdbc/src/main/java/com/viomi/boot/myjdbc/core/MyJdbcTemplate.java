package com.viomi.boot.myjdbc.core;

import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import javax.sql.DataSource;

/**
 * 数据库JDBC操作模板
 * springboot中请通过@Resource注入MyJdbcTemplate实例，dataSource则是沿用springboot提供的数据源
 * http://blog.csdn.net/q547550831/article/details/52717545
 *
 * @author ShuBifeng
 */
@Slf4j
@NoArgsConstructor
public class MyJdbcTemplate extends JdbcAccessor {

    public MyJdbcTemplate(DataSource dataSource) {
        setDataSource(dataSource);
        setName("default, 当只配了一个spring.datasource在");
    }

}