package com.viomi.boot.myjdbc.core;

import com.viomi.boot.myjdbc.exception.DbRuntimeException;
import com.viomi.boot.myjdbc.page.PageInfo;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.dbutils.BasicRowProcessor;
import org.apache.commons.dbutils.GenerousBeanProcessor;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.*;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by asus on 2018/1/31.
 */
@Getter
@Setter
@Slf4j
public abstract class JdbcAccessor implements InitializingBean {
    private final static String PAGE_SQL = " limit %d offset %d  ", SELECT_COUNT = "select count(*) ", LIMIT_RECORD = " limit 2";
    private final static String REG = "order\\s*by[\\w|\\W|\\s|\\S]*", SELECT = "select", REG2 = "\\?", FROM = "from";

    @Autowired
    private DataSource dataSource;
    private String name;

    @Override
    public void afterPropertiesSet() {
        log.info("name:{}", getName());
        if (getDataSource() == null) {
            throw new IllegalArgumentException("Property 'dataSource' is required .....");
        }
    }

    private QueryRunner getQueryRunner() {
        return new QueryRunner(getDataSource());
    }

    /**
     * 执行sql语句(insert, delete, update 语句)
     * <code>
     * executeUpdate("update user set username = 'kitty' where username = ?", "hello kitty");
     * </code>
     *
     * @param sql    sql语句
     * @param params 参数
     * @return 受影响的行数
     */
    public int update(String sql, Object... params) {
        try {
            sqlinfo(sql, params);
            return getQueryRunner().update(sql, params);
        } catch (SQLException e) {
            throw new DbRuntimeException(e);
        }
    }

    public int update(MyJdbcModel myJdbcModel) {
        return update(myJdbcModel.getSql(), myJdbcModel.getArrayParams());
    }


    public int update(MyJdbcNamedModel myJdbcModel) {
        return update(myJdbcModel.getMyJdbcModel().getSql(), myJdbcModel.getMyJdbcModel().getArrayParams());
    }

    /**
     * 执行sql语句
     *
     * @param sql    sql语句
     * @param params 参数
     * @return 受影响的行数
     */
    public int update(String sql, List<Object> params) {
        return update(sql, params.toArray());
    }

    /**
     * 执行批量sql语句
     *
     * @param sql    sql语句
     * @param params 二维参数数组
     * @return 受影响的行数的数组
     */
    public int[] batchUpdate(String sql, Object[][] params) {
        try {
            sqlinfo(sql, params);
            return getQueryRunner().batch(sql, params);
        } catch (SQLException e) {
            throw new DbRuntimeException(e);
        }
    }

    /**
     * 执行查询，将每行的结果保存到一个Map对象中，然后将所有Map对象保存到List中
     *
     * @param sql    sql语句
     * @param params 参数数组
     * @return 查询结果
     */
    @SuppressWarnings("unchecked")
    public List<Map<String, Object>> find(String sql, Object... params) {
        try {
            sqlinfo(sql, params);
            return getQueryRunner().query(sql, new MapListHandler(), params);
        } catch (SQLException e) {
            throw new DbRuntimeException(e);
        }
    }

    public List<Map<String, Object>> find(MyJdbcModel myJdbcModel) {
        return find(myJdbcModel.getSql(), myJdbcModel.getArrayParams());
    }

    public List<Map<String, Object>> find(MyJdbcNamedModel myJdbcModel) {
        return find(myJdbcModel.getMyJdbcModel().getSql(), myJdbcModel.getMyJdbcModel().getArrayParams());
    }

    /**
     * 执行查询，将每行的结果保存到一个Map对象中，然后将所有Map对象保存到List中
     *
     * @param sql    sql语句
     * @param params 参数数组
     * @return 查询结果
     */
    @SuppressWarnings("unchecked")
    public List<Map<String, Object>> find(String sql, List<Object> params) {
        return find(sql, params.toArray());
    }

    /**
     * 执行查询，将每行的结果保存到Bean中，然后将所有Bean保存到List中
     *
     * @param entityClass 类名
     * @param sql         sql语句
     * @param params      参数数组
     * @return 查询结果
     */
    @SuppressWarnings("unchecked")
    public <T> List<T> find(Class<T> entityClass, String sql, Object... params) {
        try {
            sqlinfo(sql, params);
            return (List<T>) getQueryRunner().query(sql, new BeanListHandler(entityClass, new BasicRowProcessor(new GenerousBeanProcessor())), params);
        } catch (SQLException e) {
            throw new DbRuntimeException(e);
        }
    }

    public <T> List<T> find(Class<T> entityClass, MyJdbcModel myJdbcModel) {
        return find(entityClass, myJdbcModel.getSql(), myJdbcModel.getArrayParams());
    }

    public <T> List<T> find(Class<T> entityClass, MyJdbcNamedModel myJdbcModel) {
        return find(entityClass, myJdbcModel.getMyJdbcModel().getSql(), myJdbcModel.getMyJdbcModel().getArrayParams());
    }

    /**
     * 查询单个对象,多个时异常
     *
     * @param entityClass 类名
     * @param sql         sql语句
     * @param params      参数数组
     * @param <T>
     * @return 查询结果
     */
    public <T> T findUnique(Class<T> entityClass, String sql, Object... params) {
        //LIMIT_RECORD 避免大表时意外卡死，最大返回记录数2条最佳
        List<T> list = find(entityClass, sql.concat(LIMIT_RECORD), params);
        if (list != null && !list.isEmpty()) {
            if (list.size() >= 2) {
                throw new DbRuntimeException("实际个数返回大于1个");
            }
            return list.get(0);
        } else {
            return null;
        }
    }

    public <T> T findUnique(Class<T> entityClass, MyJdbcModel myJdbcModel) {
        return findUnique(entityClass, myJdbcModel.getSql(), myJdbcModel.getArrayParams());
    }

    public <T> T findUnique(Class<T> entityClass, MyJdbcNamedModel myJdbcModel) {
        return findUnique(entityClass, myJdbcModel.getMyJdbcModel().getSql(), myJdbcModel.getMyJdbcModel().getArrayParams());
    }

    /**
     * 查询单个Map,多个时异常
     *
     * @param sql    sql语句
     * @param params 参数数组
     * @return 查询结果
     */
    public Map<String, Object> findUnique(String sql, Object... params) {
        //LIMIT_RECORD 避免大表时意外卡死，最大返回记录数2条最佳
        List<Map<String, Object>> listMap = find(sql.concat(LIMIT_RECORD), params);
        if (listMap != null && !listMap.isEmpty()) {
            if (listMap.size() >= 2) {
                throw new RuntimeException("实际个数返回大于1个");
            }
            return listMap.get(0);
        } else {
            return null;
        }
    }

    public Map<String, Object> findUnique(MyJdbcModel myJdbcModel) {
        return findUnique(myJdbcModel.getSql(), myJdbcModel.getArrayParams());
    }

    public Map<String, Object> findUnique(MyJdbcNamedModel myJdbcModel) {
        return findUnique(myJdbcModel.getMyJdbcModel().getSql(), myJdbcModel.getMyJdbcModel().getArrayParams());
    }

    /**
     * 查询出结果集中的第一条记录，并封装成对象
     *
     * @param entityClass 类名
     * @param sql         sql语句
     * @param params      参数数组
     * @return 对象
     */
    @SuppressWarnings("unchecked")
    public <T> T findFirstRow(Class<T> entityClass, String sql, Object... params) {
        try {
            sqlinfo(sql, params);
            return (T) getQueryRunner().query(sql, new BeanHandler(entityClass, new BasicRowProcessor(new GenerousBeanProcessor())), params);
        } catch (SQLException e) {
            throw new DbRuntimeException(e);
        }
    }

    public <T> T findFirstRow(Class<T> entityClass, MyJdbcModel myJdbcModel) {
        return findFirstRow(entityClass, myJdbcModel.getSql(), myJdbcModel.getArrayParams());
    }

    public <T> T findFirstRow(Class<T> entityClass, MyJdbcNamedModel myJdbcModel) {
        return findFirstRow(entityClass, myJdbcModel.getMyJdbcModel().getSql(), myJdbcModel.getMyJdbcModel().getArrayParams());
    }

    /**
     * 查询出结果集中的第一条记录，并封装成Map对象
     *
     * @param sql    sql语句
     * @param params 参数数组
     * @return 封装为Map的对象
     */
    @SuppressWarnings("unchecked")
    public Map<String, Object> findFirstRow(String sql, Object... params) {
        try {
            sqlinfo(sql, params);
            return getQueryRunner().query(sql, new MapHandler(), params);
        } catch (SQLException e) {
            throw new DbRuntimeException(e);
        }
    }

    public Map<String, Object> findFirstRow(MyJdbcModel myJdbcModel) {
        return findFirstRow(myJdbcModel.getSql(), myJdbcModel.getArrayParams());
    }

    public Map<String, Object> findFirstRow(MyJdbcNamedModel myJdbcModel) {
        return findFirstRow(myJdbcModel.getMyJdbcModel().getSql(), myJdbcModel.getMyJdbcModel().getArrayParams());
    }

    /**
     * 查询某一条记录，并将指定列的数据转换为List<?>
     *
     * @param sql        sql语句
     * @param columnName 列名
     * @param params     参数
     * @return 结果对象
     */
    public <T> List<T> findSingleColumnValue(String columnName, String sql, Object... params) {
        try {
            sqlinfo(sql, params);
            ColumnListHandler columnListHandler = new ColumnListHandler(columnName);
            return (List<T>) getQueryRunner().query(sql, columnListHandler, params);
        } catch (SQLException e) {
            throw new DbRuntimeException(e);
        }
    }

    public <T> List<T> findSingleColumnValue(String columnName, MyJdbcModel myJdbcModel) {
        return findSingleColumnValue(columnName, myJdbcModel.getSql(), myJdbcModel.getArrayParams());
    }

    public <T> List<T> findSingleColumnValue(String columnName, MyJdbcNamedModel myJdbcModel) {
        return findSingleColumnValue(columnName, myJdbcModel.getMyJdbcModel().getSql(), myJdbcModel.getMyJdbcModel().getArrayParams());
    }

    /**
     * 查询第一行且某一列的数据
     * select count(*) from user;
     *
     * @param sql        sql语句
     * @param columnName 列名
     * @param params     参数数组
     * @return 结果对象
     */
    public <T> T getSingleValue(String columnName, String sql, Object... params) {
        try {
            sqlinfo(sql, params);
            return (T) getQueryRunner().query(sql, new ScalarHandler(columnName), params);
        } catch (SQLException e) {
            throw new DbRuntimeException(e);
        }
    }

    public <T> T getSingleValue(String columnName, MyJdbcModel myJdbcModel) {
        return getSingleValue(columnName, myJdbcModel.getSql(), myJdbcModel.getArrayParams());
    }

    public <T> T getSingleValue(String columnName, MyJdbcNamedModel myJdbcModel) {
        return getSingleValue(columnName, myJdbcModel.getMyJdbcModel().getSql(), myJdbcModel.getMyJdbcModel().getArrayParams());
    }

    /**
     * 查询第一行的某一列记录
     *
     * @param sql         sql语句(通常用在select a,b,c,d from user选择一列索引作为返回，且仅获取第一行对应的值)
     * @param columnIndex 列索引(严禁select*from时使用)
     * @param params      参数数组
     * @return 结果对象
     */
    public <T> T getSingleValue(int columnIndex, String sql, Object... params) {
        try {
            sqlinfo(sql, params);
            return (T) getQueryRunner().query(sql, new ScalarHandler(columnIndex), params);
        } catch (SQLException e) {
            throw new DbRuntimeException(e);
        }
    }

    public <T> T getSingleValue(int columnIndex, MyJdbcModel myJdbcModel) {
        return getSingleValue(columnIndex, myJdbcModel.getSql(), myJdbcModel.getArrayParams());
    }

    public <T> T getSingleValue(int columnIndex, MyJdbcNamedModel myJdbcModel) {
        return getSingleValue(columnIndex, myJdbcModel.getMyJdbcModel().getSql(), myJdbcModel.getMyJdbcModel().getArrayParams());
    }

    /**
     * 查询第一行的某一列记录
     *
     * @param sql    sql语句(通常用在select count(*) from user等单个值)
     * @param params 参数数组
     * @return 结果对象
     */
    public <T> T getSingleValue(String sql, Object... params) {

        try {
            sqlinfo(sql, params);
            return (T) getQueryRunner().query(sql, new ScalarHandler(), params);
        } catch (SQLException e) {
            throw new DbRuntimeException(e);
        }
    }

    public <T> T getSingleValue(MyJdbcModel myJdbcModel) {
        return getSingleValue(myJdbcModel.getSql(), myJdbcModel.getArrayParams());
    }

    public <T> T getSingleValue(MyJdbcNamedModel myJdbcModel) {
        return getSingleValue(myJdbcModel.getMyJdbcModel().getSql(), myJdbcModel.getMyJdbcModel().getArrayParams());
    }

    /**
     * 分页查询返回map
     *
     * @param pageNo
     * @param pageSize
     * @param sql
     * @param params
     * @return 分页结果对象
     */
    public PageInfo<Map<String, Object>> page(int pageNo, int pageSize, String sql, Object... params) {
        return page(null, pageNo, pageSize, sql, params);
    }

    public PageInfo<Map<String, Object>> page(MyJdbcPModel myJdbcPModel) {
        return page(null, myJdbcPModel.getPageNo(), myJdbcPModel.getPageSize(), myJdbcPModel.getSql(), myJdbcPModel.getArrayParams());
    }

    public PageInfo<Map<String, Object>> page(MyJdbcPNamedModel myJdbcPModel) {
        return page(null, myJdbcPModel.getPageNo(), myJdbcPModel.getPageSize(), myJdbcPModel.getMyJdbcModel().getSql(), myJdbcPModel.getMyJdbcModel().getArrayParams());
    }

    /**
     * 分页查询返回实体
     *
     * @param pageNo
     * @param pageSize
     * @param sql
     * @param params
     * @return 分页结果对象
     */
    public <T> PageInfo<T> page(Class<T> entityClass, int pageNo, int pageSize, String sql, Object... params) {
        AtomicInteger atomic = new AtomicInteger(0);
        String sqlFrom = removeSelect(removeOrders(sql), atomic);
        Long totalCount = 0L;
        PageInfo pageResult = new PageInfo(pageNo, pageSize);
        String countSqlString = SELECT_COUNT.concat(sqlFrom);
        if (atomic.intValue() > 0 && params != null) {
            Object[] newParams = Arrays.copyOfRange(params, atomic.intValue(), params.length);
            totalCount = getSingleValue(countSqlString, newParams);
        } else {
            totalCount = getSingleValue(countSqlString, params);
        }
        pageResult.setTotalCount(totalCount);
        log.debug("page----> totalCount:{} ", totalCount);
        if (totalCount <= 0) {
            pageResult.setList(Collections.EMPTY_LIST);
            return pageResult;
        }
        log.debug("page----> pageNo:{} pageSize:{} ", pageNo, pageSize);
        //postgresql分页
        int offset = (pageNo - 1) * pageSize;
        int limit = pageSize;
        if (entityClass == null || entityClass == Map.class || entityClass == HashMap.class) {
            List<Map<String, Object>> lstMap = find(sql.concat(String.format(PAGE_SQL, limit, offset)), params);
            pageResult.setList(lstMap);
        } else {
            List<T> lstEntity = find(entityClass, sql.concat(String.format(PAGE_SQL, limit, offset)), params);
            pageResult.setList(lstEntity);
        }
        return pageResult;
    }

    public <T> PageInfo<T> page(Class<T> entityClass, MyJdbcPModel myJdbcPModel) {
        return page(entityClass, myJdbcPModel.getPageNo(), myJdbcPModel.getPageSize(), myJdbcPModel.getSql(), myJdbcPModel.getArrayParams());
    }

    public <T> PageInfo<T> page(Class<T> entityClass, MyJdbcPNamedModel pageModel) {
        return page(entityClass, pageModel.getPageNo(), pageModel.getPageSize(), pageModel.getMyJdbcModel().getSql(), pageModel.getMyJdbcModel().getArrayParams());
    }
    /////////////////////////////////////// 辅助方法////////////////////////

    /***
     * 截取复杂的子查询，防止查询记录总数时报错
     *
     * @param qlString
     * @param selectIndex
     * @param fromIndex
     * @return
     */
    private int addSelRem(String qlString, int selectIndex, int fromIndex) {
        selectIndex = qlString.indexOf(SELECT, selectIndex) + 1;
        fromIndex = qlString.indexOf(FROM, fromIndex) + 1;
        if (selectIndex > fromIndex || selectIndex == 0) {
            // 直接返回传入值
            return fromIndex;
        } else if (fromIndex > selectIndex) {
            // 继续向下寻找
            return addSelRem(qlString, selectIndex, fromIndex);
        } else {
            return fromIndex;
        }
    }

    /**
     * 去除sql的orderBy子句。
     *
     * @param sqlString
     * @return
     */
    private String removeOrders(String sqlString) {
        Pattern p = Pattern.compile(REG, Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(sqlString);
        StringBuffer sb = new StringBuffer();
        while (m.find()) {
            m.appendReplacement(sb, "");
        }
        m.appendTail(sb);
        return sb.toString();
    }

    /**
     * 去除qlString的select子句。
     *
     * @param qlString
     * @param atomic   统计"?"出现的次数
     * @return
     */
    private String removeSelect(String qlString, AtomicInteger atomic) {
        qlString = qlString.toLowerCase();
        int n = addSelRem(qlString, qlString.indexOf(SELECT) + 1, 0);

        Pattern pattern = Pattern.compile(REG2);
        Matcher matcher = pattern.matcher(qlString.substring(0, n));
        int cn = 0;
        while (matcher.find()) {
            cn++;// 知道查询字段中出现的参数个数
        }
        atomic.set(cn);
        return qlString.substring(n - 1);
    }

    /**
     * sql参数解析
     *
     * @param sql
     * @param params
     */
    private void sqlinfo(String sql, Object... params) {
        if (log.isDebugEnabled()) {
            log.debug("jdbc ---->    sql:{}", sql);
            log.debug("jdbc ----> params:{}", Arrays.deepToString(params));
        }
    }
}
