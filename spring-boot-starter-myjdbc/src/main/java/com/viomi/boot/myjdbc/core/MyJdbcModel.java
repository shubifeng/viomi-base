package com.viomi.boot.myjdbc.core;

import lombok.*;

import java.util.List;

/**
 * @author Shubifeng
 */
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MyJdbcModel {

    @Getter
    @Setter
    private String sql;

    @Getter
    private Object[] arrayParams;

    public void setArrayParams(List<Object> listParams) {
        this.arrayParams = listParams.toArray();
    }

    public void setArrayParams(Object... arrayParams) {
        this.arrayParams = arrayParams;
    }
}
