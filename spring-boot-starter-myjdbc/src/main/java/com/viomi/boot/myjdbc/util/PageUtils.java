package com.viomi.boot.myjdbc.util;

/**
 * @author ShuBifeng
 * @date 2018-01-04
 */
public class PageUtils {

    public static int getNowPage(Integer page, int totalPageNum) {
        int nowPage = page != null && page > 0 ? page : 1;
        if (page != null && page > totalPageNum) {
            return totalPageNum;
        }
        return nowPage;
    }

    public static int getTotalPageNum(int pageSize, long totalCount) {

        if (totalCount != 0) {
            return (int) (totalCount % pageSize == 0 ? totalCount / pageSize
                    : totalCount / pageSize + 1);
        }
        // count为0计为有一页
        return 1;
    }

    public static int getPageSize(Integer size) {
        return size != null && size > 0 ? size : 10;
    }

}
