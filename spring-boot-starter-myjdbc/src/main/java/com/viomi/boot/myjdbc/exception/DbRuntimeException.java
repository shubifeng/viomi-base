package com.viomi.boot.myjdbc.exception;

/**
 * 数据库操作异常
 *
 * @author Shubifeng
 */
public class DbRuntimeException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public DbRuntimeException() {
        super();
    }

    public DbRuntimeException(Throwable cause) {
        super(cause);
    }

    public DbRuntimeException(String msg) {
        super(msg);
    }

    public DbRuntimeException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public static void assertNotNull(Object object, String msg) {
        if (object == null) {
            throw new DbRuntimeException(msg);
        }
    }
}
