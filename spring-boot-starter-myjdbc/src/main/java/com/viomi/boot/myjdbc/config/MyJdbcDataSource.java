package com.viomi.boot.myjdbc.config;

import com.viomi.boot.myjdbc.core.MyJdbcTemplate;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

/**
 * @author shubifeng.
 */
@Configuration
@ConditionalOnClass({DataSource.class})
@ConditionalOnBean(DataSource.class)
@Slf4j
public class MyJdbcDataSource {

//    @Primary
//    @Bean
//    @ConfigurationProperties(prefix = "spring.datasource")
//    public DataSource oneDataSource() {
//        log.info("dataSource 创建");
//        return DataSourceBuilder.create().build();
//    }

//    @Bean
//    public MyJdbcTemplate init(@Autowired DataSource dataSource) {
//        log.info("MyJdbcDataSource 注入");
//        return new MyJdbcTemplate(dataSource);
//    }

    @Bean
    public MyJdbcTemplate init() {
        log.info("MyJdbcDataSource 注入");
        return new MyJdbcTemplate();
    }
    /*@Primary
    @Bean(name = "dataSource")
    @Qualifier("dataSource")
    @ConfigurationProperties(prefix = "spring.datasource")
    @ConditionalOnClass(name = {"com.mysql.jdbc.Driver"})
    public DataSource oneDataSource() {
        log.info("dataSource 创建");
        return DataSourceBuilder.create().build();
    }

    @Bean(name = "gpDataSource")
    @Qualifier("gpDataSource")
    @ConfigurationProperties(prefix = "spring.datasource.druid.gp")
    @ConditionalOnClass(name = {"org.postgresql.Driver"})
    public DataSource gpDataSource() {
        log.info("gpDataSource 创建");
        return DataSourceBuilder.create().build();
    }

    @Bean("myJdbcTemplate")
    @ConditionalOnBean(name = {"dataSource"})
    public MyJdbcTemplate myJdbcTemplate(@Qualifier("dataSource") DataSource dataSource) {
        log.info("dataSource 注入MyJdbcTemplate (msyql 数据源 )");
        return new MyJdbcTemplate(dataSource);
    }

    @Bean("gpJdbcTemplate")
    @ConditionalOnBean(name = {"gpDataSource"})
    public GpJdbcTemplate gpJdbcTemplate(@Qualifier("gpDataSource") DataSource dataSource) {
        log.info("gpDataSource 注入GpJdbcTemplate( greenplum 或者 postgresql 数据源)");
        return new GpJdbcTemplate(dataSource);
    }
*/
}
