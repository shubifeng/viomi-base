package com.viomi.boot.myjdbc.core;

import com.viomi.boot.myjdbc.namedparam.MapSqlParameterSource;
import com.viomi.boot.myjdbc.namedparam.NamedParameterUtils;
import com.viomi.boot.myjdbc.namedparam.ParsedSql;
import com.viomi.boot.myjdbc.namedparam.SqlParameterSource;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Shubifeng
 */
@ToString
@NoArgsConstructor
@Builder
@Slf4j
public class MyJdbcNamedModel {

    @Setter
    private String sql;

    @Setter
    private Map<String, ?> mapParams;

    public MyJdbcNamedModel(String sql, Map<String, ?> mapParams) {
        this.sql = sql;
        this.mapParams = mapParams;
    }

    public MyJdbcModel getMyJdbcModel() {
        SqlParameterSource paramSource = new MapSqlParameterSource(mapParams);
        ParsedSql parsedSql = NamedParameterUtils.parseSqlStatement(sql);
        String sqlToUse = NamedParameterUtils.substituteNamedParameters(parsedSql, paramSource);
        Object[] params = NamedParameterUtils.buildValueArray(parsedSql, paramSource, null);
        //List<SqlParameter> declaredParameters = NamedParameterUtils.buildSqlParameterList(parsedSql, paramSource);
        if (log.isTraceEnabled()) {
            log.trace("===========================================@========================================================");
            log.trace("originalSql : {}", parsedSql);
            log.trace("orgparam: {}", mapParams);
            log.trace("===========================================@@@@@@====================================================");
        }
        //in查询转换
        List<Object> list = new ArrayList<>(10);
        for (int i = 0; i < params.length; i++) {
            Object obj = params[i];
            if (obj instanceof Object[]) {
                Object[] valueArray = ((Object[]) obj);
                for (Object argValue : valueArray) {
                    list.add(argValue);
                }
            } else if (obj instanceof Iterable) {
                Iterable<Object> iter = ((Iterable) obj);
                iter.forEach(o -> {
                    list.add(o);
                });
            } else {
                list.add(obj);
            }
        }

        MyJdbcModel myJdbcModel = new MyJdbcModel();
        myJdbcModel.setArrayParams(list);
        myJdbcModel.setSql(sqlToUse);
        return myJdbcModel;
    }
}
