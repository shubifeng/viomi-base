package com.viomi.boot.myjdbc.util;

import org.apache.commons.dbutils.QueryLoader;

import java.io.IOException;
import java.util.Map;

/**
 * Created by asus on 2018/1/31.
 */
public class SqlMap {

    public static Map<String, String> loadQueries(String path) {
        QueryLoader ql = QueryLoader.instance();
        try {
            return ql.load(path);
        } catch (IOException e) {
            throw new RuntimeException("Load Queries  fail, [" + path + "]");
        }
    }
}
