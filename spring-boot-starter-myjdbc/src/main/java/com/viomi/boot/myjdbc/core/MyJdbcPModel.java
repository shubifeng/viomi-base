package com.viomi.boot.myjdbc.core;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Shubifeng
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MyJdbcPModel extends MyJdbcModel {

    private int pageSize;

    private int pageNo;

}
