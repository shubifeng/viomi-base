package com.viomi.boot.myjdbc.page;

import com.viomi.boot.myjdbc.util.PageUtils;
import lombok.Data;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 * @author ShuBifeng
 * @date 2018-01-04
 */
@Data
public class PageInfo<T> implements Serializable {

    private Integer pageNum;

    private Integer pageSize;

    private Long totalCount;

    private Integer totalPageNum;

    private List<T> list;

    public PageInfo(Integer nowPage, Integer pageSize, Long totalCount, List<T> list) {
        this.pageSize = pageSize;
        this.totalCount = totalCount;
        this.totalPageNum = PageUtils.getTotalPageNum(pageSize, totalCount);
        this.pageNum = PageUtils.getNowPage(nowPage, this.totalPageNum);
        this.list = list;
    }

    public PageInfo(Integer nowPage, Integer pageSize) {
        this.pageSize = pageSize;
        this.pageNum = nowPage;
    }

    public void setTotalCount(Long totalCount) {
        if (Objects.isNull(totalCount)) {
            totalCount = 0L;
        }
        this.totalCount = totalCount;
        this.totalPageNum = PageUtils.getTotalPageNum(pageSize, totalCount);
    }

}
