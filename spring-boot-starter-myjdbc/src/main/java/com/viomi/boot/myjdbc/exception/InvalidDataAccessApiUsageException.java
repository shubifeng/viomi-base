package com.viomi.boot.myjdbc.exception;

/**
 * Created by asus on 2018/2/1.
 */
public class InvalidDataAccessApiUsageException extends RuntimeException {
    public InvalidDataAccessApiUsageException(String msg) {
        super(msg);
    }

    public InvalidDataAccessApiUsageException(String msg, Throwable cause) {
        super(msg, cause);
    }
}
