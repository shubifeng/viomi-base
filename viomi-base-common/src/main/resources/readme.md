
##当前工程主要用于纳入被抽离的公共代码，具有复用性，规范性，强制性。由于是基础公共代码，修改时必须保证向后兼容。避免发布开发测试生产引用时出现问题，减少版本管理的成本。




####以下具体介绍各个包的定义：

com.viomi.common 主路径

  constant 存放常量相关的 如接口统一返回错误码，枚举，redis的key等
  
  exception 自定义通用异常相关  
   
####pom.xml指定常使用的依赖maven包，其他各个项目引用时就无须再额外指定maven包版本，便于统一技术栈版本管理。
如guava，apache-common-util等。

