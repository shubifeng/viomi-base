package com.viomi.base.common.model;

import com.viomi.base.common.constant.TimeTypeConst;
import lombok.Getter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import java.sql.Timestamp;
import java.util.Date;

/**
 * 日期参数转换
 *
 * @author Shubifeng
 */
@Slf4j
@Getter
@ToString
public final class ParamDate {


    /**
     * 开始时间
     */
    private Date beginDate;
    /**
     * 结束时间
     */
    private Date endDate;
    /**
     * 开始时间
     */
    private String beginDateStr;
    /**
     * 结束时间
     */
    private String endDateStr;
    /**
     * 数据库日期格式化模板to_char(timestamp,'pgDateFormat就是我')
     */
    private String pgDateFormat;
    /**
     * 原始时间类型
     */
    private TimeTypeConst orgTimeType;
    /**
     * 原始开始时间值
     */
    private long orgBeginTime;
    /**
     * 原始结束时间值
     */
    private long orgEndTime;

    public ParamDate(Date beginDate, Date endDate, String pgDateFormat, TimeTypeConst orgTimeType, long orgBeginTime, long orgEndTime, String beginDateStr, String endDateStr) {
        this.beginDate = beginDate;
        this.endDate = endDate;
        this.pgDateFormat = pgDateFormat;
        this.orgTimeType = orgTimeType;
        this.orgBeginTime = orgBeginTime;
        this.orgEndTime = orgEndTime;
        this.beginDateStr = beginDateStr;
        this.endDateStr = endDateStr;
    }


    /**
     * postgresql需使用timestamp
     *
     * @return
     */
    public Timestamp getGpBeginDate() {
        return new java.sql.Timestamp(beginDate.getTime());
    }

    /**
     * postgresql需使用timestamp
     *
     * @return
     */
    public Timestamp getGpEndDate() {
        return new java.sql.Timestamp(endDate.getTime());
    }

    /**
     * 开始时间戳
     *
     * @return
     */
    public long getBegintime() {
        return beginDate.getTime() / 1000;
    }

    /**
     * 结束时间戳
     *
     * @return
     */
    public long getEndtime() {
        return endDate.getTime() / 1000;
    }

}
