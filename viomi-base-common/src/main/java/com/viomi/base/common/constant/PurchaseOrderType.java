package com.viomi.base.common.constant;

/**
 * 平台类型
 *
 * @author refer to vstore PurchaseOrderType
 */
public enum PurchaseOrderType{
    //
    VIOMI(1, "云米产品提货"),
    //
    MI(2, "小米生态链产品提货"),
    //
    MATERIEL(3, "物料提货");

    private int code;

    private String desc;

    private PurchaseOrderType(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static PurchaseOrderType getByCode(int code)
    {
        PurchaseOrderType s = VIOMI;
        for(PurchaseOrderType e : PurchaseOrderType.values())
        {
            if(e.code == code)
            {
                s = e;
                break;
            }
        }
        return s;
    }

    public int getCode()
    {
        return code;
    }

    public String getDesc()
    {
        return desc;
    }


}
