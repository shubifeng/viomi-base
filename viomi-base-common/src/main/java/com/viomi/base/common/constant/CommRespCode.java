package com.viomi.base.common.constant;

/**
 * 通用web层接口返回码
 * TODO 建议统一规划code范围，如负数为系统异常，0位正常，0-10000属公共代码占有，10000-19999订单系统业务编码，20000-29999商品管理业务编码，30000-39999会员模块编码
 *
 * @author ShuBifeng
 * @date 2018-01-04
 */
public enum CommRespCode implements Code {
    SUCCESS(100, "OK"),
    SOFTWARE_UPDATE(162, "客户端强制升级"),
    ORDER_VALIDATE_EXCEPTION(604, "订单校验失败"),
    ORDER_EXISTS(620, "订单已存在"),
    USER_EXISTS(711, "用户已存在"),
    USER_NOT_BIND(712, "用户已存在，但未绑定当前系统"),
    USER_NOT_EXISTS(713, "用户不存在"),
    FILE_IMPORT_FAIL(818, "文件导入失败"),
    SMS_SEND_FAIL(819, "短信发送失败"),
    CALL_FAIL(820, "接口调用失败"),
    SERVER_EXCEPTION(900, "服务器异常"),
    BUSINESS_REQPARAM_EXCEPTION(901, "非法请求参数"),
    DATA_NOT_FOUND(902, "无数据记录"),
    BUSINESS_EXCEPTION(915, "业务异常"),
    JSON_PARSE_EXCEPTION(930, "json解析异常"),
    TOKEN_ABSENT_EXCEPTION(918, "token值为空或无效"),
    TOKEN_EXPIRED_EXCEPTION(919, "登录已过期"),
    FEEDBACK_PROCESSED(920, "该反馈已处理"),
    GENERATE_REPORT_FAILED(1100, "生成报表失败"),
    GENERATED_RESULT_EMPTY(1101, "生成报表失败"),

    /**
     * 业务处理失败
     */
    FAIL(-1, "处理失败"),

    /**
     * 业务处理失败
     */
    PROGRAM_EXCEPTION(-3, "程序异常"),

    /**
     * 业务处理失败
     */
    HTTP_EXCEPTION(-4, "http调用异常"),

    /**
     * 业务处理失败
     */
    RPC_EXCEPTION(-5, "rpc调用异常");




    private Integer code;
    private String desc;

    CommRespCode(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    @Override
    public Integer getCode() {
        return this.code;
    }

    @Override
    public String getDesc() {
        return this.desc;
    }

}
