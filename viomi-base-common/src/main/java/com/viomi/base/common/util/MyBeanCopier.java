package com.viomi.base.common.util;

import net.sf.cglib.beans.BeanCopier;
import net.sf.cglib.core.Converter;
import org.apache.commons.collections.CollectionUtils;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


/**
 * 功能简述:Entity与Model间转换默认实现类<br>
 *
 * @author ShuBifeng
 * @date 2018-01-04
 */
public class MyBeanCopier {

    /**
     * 功能简述:简单的只对target与source间的字段转换<br>
     *
     * @param target
     * @param source
     * @return
     */
    public static <T> T assemble(Class<T> target, Object source) {
        return assemble(target, source, null, null);
    }

    /**
     * 功能简述:target与source间的字段转换，提供对特殊字段的类型转换<br>
     *
     * @param target
     * @param source
     * @param converter
     * @return
     */
    public static <T> T assemble(Class<T> target, Object source, Converter converter) {
        return assemble(target, source, null, converter);
    }

    /**
     * 功能简述:分页对象转换<br>
     *
     * @param target
     * @param sources
     * @return
     * @see [类、类#方法、类#成员]

     @SuppressWarnings({"unchecked", "rawtypes"})
     public static <T> PageInfo<T> pageAssemble(Class<T> target, List sources) {
     PageInfo<T> targets = new PageInfo<T>(sources);
     targets.setList(multiAssemble(target, sources, null, null));
     return targets;
     }*/

    /**
     * 功能简述:分页对象转换<br>
     *
     * @param target
     * @param sources
     * @return
     * @see [类、类#方法、类#成员]

     @SuppressWarnings({"unchecked", "rawtypes"})
     public static <T> ResultPage<T> pageAssemble2(Class<T> target, List sources) {
     PageInfo<T> targets = new PageInfo<T>(sources);
     targets.setList(multiAssemble(target, sources, null, null));
     return ResultPage.ok(targets);
     }
     */
    /**
     * 功能简述:target与source间的字段转换，提供钩子函数<br>
     *
     * @param target
     * @param source
     * @param callBack
     * @return
     */
    public static <T> T assemble(Class<T> target, Object source, AssemblerCallBack callBack) {
        return assemble(target, source, callBack, null);
    }

    /**
     * 功能简述:target与source间的字段转换，提供钩子函数和类型转换<br>
     *
     * @param target
     * @param source
     * @param callBack
     * @param converter
     * @return
     */
    public static <T> T assemble(Class<T> target, Object source, AssemblerCallBack callBack, Converter converter) {
        if (source == null) {
            return null;
        }
        try {
            boolean isUseConverter = false;
            if (converter != null) {
                isUseConverter = true;
            }
            T t = target.newInstance();
            BeanCopier beanCopier = BeanCopier.create(source.getClass(), target, isUseConverter);
            beanCopier.copy(source, t, converter);
            if (callBack != null) {
                callBack.afterAssembler(t, source);
            }
            return t;
        } catch (Exception e) {
            throw new RuntimeException("create object fail, class:" + target.getName() + " ", e);
        }
    }

    /**
     * 功能简述:将集合sources中的对象转换为target类型<br>
     *
     * @param target
     * @param sources
     * @return
     */
    public static <T> List<T> multiAssemble(Class<T> target, Collection<?> sources) {
        return multiAssemble(target, sources, null, null);
    }

    /**
     * 功能简述:将集合sources中的对象转换为target类型，提供对特殊字段的类型转换<br>
     * 详细描述:<br>
     *
     * @param target
     * @param sources
     * @param converter
     * @return
     */
    public static <T> List<T> multiAssemble(Class<T> target, Collection<?> sources, Converter converter) {
        return multiAssemble(target, sources, null, converter);
    }

    /**
     * 功能简述:将集合sources中的对象转换为target类型，提供钩子函数<br>
     *
     * @param target
     * @param sources
     * @param callBack
     * @return
     */
    public static <T> List<T> multiAssemble(Class<T> target, Collection<?> sources, AssemblerCallBack callBack) {
        return multiAssemble(target, sources, callBack, null);
    }

    /**
     * 功能简述:将集合sources中的对象转换为target类型，提供钩子函数和类型转换<br>
     *
     * @param target
     * @param sources
     * @param callBack
     * @param converter
     * @return
     */
    public static <T> List<T> multiAssemble(Class<T> target, Collection<?> sources, AssemblerCallBack callBack, Converter converter) {
        List<T> targets = new ArrayList();
        if (CollectionUtils.isEmpty(sources)) {
            return targets;
        }

        try {
            boolean isUseConverter = false;
            if (converter != null) {
                isUseConverter = true;
            }
            BeanCopier beanCopier = BeanCopier.create(sources.toArray()[0].getClass(), target, isUseConverter);
            for (Object object : sources) {
                T t = target.newInstance();
                beanCopier.copy(object, t, converter);
                if (callBack != null) {
                    callBack.afterAssembler(t, object);
                }
                targets.add(t);
            }
        } catch (Exception e) {
            throw new RuntimeException("create object fail, class:" + target.getName() + " ", e);
        }
        return targets;
    }

    /**
     * 实体bean对象转换成DBObject
     *
     * @param bean
     * @return
     * @throws IllegalArgumentException
     * @throws IllegalAccessException

    public static <T> DBObject bean2DBObject(T bean) throws IllegalArgumentException,
            IllegalAccessException {
        if (bean == null) {
            return null;
        }
        DBObject dbObject = new BasicDBObject();
        // 获取对象对应类中的所有属性域
        Field[] fields = bean.getClass().getDeclaredFields();
        for (Field field : fields) {
            // 获取属性名
            String varName = field.getName();
            // 修改访问控制权限
            boolean accessFlag = field.isAccessible();
            if (!accessFlag) {
                field.setAccessible(true);
            }
            Object param = field.get(bean);
            if (param == null) {
                continue;
            } else if (param instanceof Integer) {
                // 判断变量的类型
                int value = ((Integer) param).intValue();
                dbObject.put(varName, value);
            } else if (param instanceof String) {
                String value = (String) param;
                dbObject.put(varName, value);
            } else if (param instanceof Double) {
                double value = ((Double) param).doubleValue();
                dbObject.put(varName, value);
            } else if (param instanceof Float) {
                float value = ((Float) param).floatValue();
                dbObject.put(varName, value);
            } else if (param instanceof Long) {
                long value = ((Long) param).longValue();
                dbObject.put(varName, value);
            } else if (param instanceof Boolean) {
                boolean value = ((Boolean) param).booleanValue();
                dbObject.put(varName, value);
            } else if (param instanceof Date) {
                Date value = (Date) param;
                dbObject.put(varName, value);
            } else if (param instanceof Byte) {
                Byte value = (Byte) param;
                dbObject.put(varName, value);
            } else if (param instanceof BasicDBObject) {
                BasicDBObject value = (BasicDBObject) param;
                dbObject.put(varName, value);
            } else if (param instanceof BasicDBList) {
                BasicDBList value = (BasicDBList) param;
                dbObject.put(varName, value);
            } else {
                throw new IllegalArgumentException("Cant handle param for instanceof:" + param.getClass());
            }
            // 恢复访问控制权限
            field.setAccessible(accessFlag);
        }
        return dbObject;
    }
     */

    /**
     * DBObject转实体bean对象
     *
     * @param dbObject
     * @param bean
     * @return
     * @throws IllegalAccessException
     * @throws InvocationTargetException
     * @throws NoSuchMethodException

    public static <T> T dbObject2Bean(DBObject dbObject, T bean) throws IllegalAccessException,
            InvocationTargetException, NoSuchMethodException {
        if (bean == null) {
            return null;
        }
        Field[] fields = bean.getClass().getDeclaredFields();
        for (Field field : fields) {
            String varName = field.getName();
            Object object = dbObject.get(varName);
            if (object != null) {
                BeanUtils.setProperty(bean, varName, object);
            }
        }
        return bean;
    }
     */
    /**
     * 回调
     */
    interface AssemblerCallBack {
        void afterAssembler(Object target, Object source);
    }
}
