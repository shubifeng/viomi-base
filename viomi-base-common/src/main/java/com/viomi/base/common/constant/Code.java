package com.viomi.base.common.constant;

/**
 * 接口返回码父接口（抽取公共父接口是为了便于子系统去扩展自己的响应code，而ResponseCode.java这里的定义的是通用常用编码）
 *
 * @author ShuBifeng
 * @date 2018-01-04
 */
public interface Code {

    /**
     * 获取编码
     *
     * @return
     */
    Integer getCode();

    /**
     * 获取编码描述
     *
     * @return
     */
    String getDesc();
}
