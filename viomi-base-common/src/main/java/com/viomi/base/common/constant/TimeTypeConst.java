package com.viomi.base.common.constant;

import com.viomi.base.common.exception.CommonException;

/**
 * 时间类型
 *
 * @author shubifeng
 */
public enum TimeTypeConst{
    day('d'),
    month('m'),
    quart('q'),
    year('y');
    private char type;

    TimeTypeConst(char type) {
        this.type = type;
    }

    public static TimeTypeConst get(char type) {
        for (TimeTypeConst c : TimeTypeConst.values()) {
            if (c.type == type) {
                return c;
            }
        }
        throw new CommonException("无效的timeType值");
    }


    public String getType() {
        return String.valueOf(type);
    }
}
