package com.viomi.base.common.constant;

import com.viomi.base.common.exception.CommonException;

/**
 * 平台类型
 *
 * @author shubifeng
 */
public enum PlatformTypeConst{
    YUN_MI(1, "云米商城"),
    MI_JIA(2, "米家商城"),
    TIAN_MAO(3, "天猫商城"),
    YU_FU_KUAN(4, "预付款提货");
    private int type;
    private String desc;

    PlatformTypeConst(int type, String desc) {
        this.type = type;
        this.desc = desc;
    }

    public static PlatformTypeConst get(int type) {
        for (PlatformTypeConst c : PlatformTypeConst.values()) {
            if (c.type() == type) {
                return c;
            }
        }
        throw new CommonException("无效的platformType值");
    }

    public static PlatformTypeConst getByType(int type) {
        PlatformTypeConst ret = YUN_MI;
        for (PlatformTypeConst c : PlatformTypeConst.values()) {
            if (c.type() == type) {
                ret = c;
            }
        }
        return ret;
    }

    public int type() {
        return type;
    }

    public String desc() {
        return desc;
    }


}
