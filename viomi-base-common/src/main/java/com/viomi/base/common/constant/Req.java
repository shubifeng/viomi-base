package com.viomi.base.common.constant;

/**
 * 通用web层数据返回格式
 *
 * @author ShuBifeng
 * @date 2018-01-04
 */
public interface Req {

    String platformType = "platformType";
    String platformTypeDesc = "平台类型：YUN_MI (1云米商城), MI_JIA (2米家商城), TIAN_MAO (3天猫商城), YU_FU_KUAN (4预付款提货)";
    String purchaseOrderType = "orderType";
    String purchaseOrderDesc = "预付款订单类型：YUN_MI (1云米产品提货), XIAO_MI (2小米生态链产品提货), MATERIAL (3物料提货)";
    String timeType = "timeType";
    String beginTime = "beginTime";
    String endTime = "endTime";
    String timeDesc = "timeType可选值: day(d按天,默认),month(m按月),quart(q按季),year(y按年)";
    String pageNum = "pageNum";
    String pageSize = "pageSize";

}
