package com.viomi.base.common.exception;

import com.viomi.base.common.constant.Code;
import com.viomi.base.common.constant.CommRespCode;
import lombok.Data;

/**
 * 通用扩展自定义异常
 *
 * @author ShuBifeng
 * @date 2018-01-04
 */
@Data
public class CommonException extends RuntimeException {

    private Code commCode = CommRespCode.FAIL;

    public CommonException() {
    }

    public CommonException(String message) {
        super(message);
    }

    public CommonException(Throwable cause) {
        super(cause);
    }

    public CommonException(String message, Throwable cause) {
        super(message, cause);
    }

    public CommonException(String msgTemplete, Object... objs) {
        super(String.format(msgTemplete, objs));
    }

    public CommonException(Code msgCode) {
        super(msgCode.getDesc());
        this.commCode = msgCode;
    }
}
