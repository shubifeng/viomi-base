package com.viomi.base.common.restapi;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.convert.converter.Converter;
import org.springframework.core.convert.converter.ConverterFactory;

/**
 * @author Shubifeng
 */
@Slf4j
public class StringToBaseEnumConverterFactory implements ConverterFactory<String, IBaseEnum> {
    private final static String TIPS = "只支持转换到枚举类型";
    @Override
    public <T extends IBaseEnum> Converter<String, T> getConverter(Class<T> targetType) {
        if (!targetType.isEnum()) {
            throw new UnsupportedOperationException(TIPS);
        }
        return new StringToBaseEnumConverter(targetType);
    }

    private class StringToBaseEnumConverter<T extends IBaseEnum> implements Converter<String, T> {
        private final Class<T> enumType;

        public StringToBaseEnumConverter(Class<T> enumType) {
            this.enumType = enumType;
        }

        @Override
        public T convert(String s) {
            for (T t : enumType.getEnumConstants()) {
                if (s.equals(t.getName())) {
                    return t;
                }
            }
            throw new IllegalArgumentException();
        }
    }
}
