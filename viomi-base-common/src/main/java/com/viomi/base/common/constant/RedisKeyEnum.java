package com.viomi.base.common.constant;

import org.apache.commons.lang3.StringUtils;

/**
 * redis使用时的key定义
 *
 * @author ShuBifeng
 * @date 2018-01-04
 */
public enum RedisKeyEnum {
    /**
     * 统一授权用户信息key
     */
    UNION_AUTH_USER("union-auth", 90 * 24 * 60 * 60 * 1000L);

    private final static String REDIS_ROOT = "vm_";

    private long seconds;
    private String key;
    private String redisKey;

    RedisKeyEnum(String key, long seconds) {
        this.seconds = seconds;
        this.key = key;
    }

    public long seconds() {
        return this.seconds;
    }

    public String key() {
        return this.key;
    }

    /**
     * 获取redis全路径key
     *
     * @param paramKey 不要带:号
     * @return
     */
    public String redisKey(Object... paramKey) {
        if (paramKey != null && paramKey.length > 0) {
            redisKey = StringUtils.join(paramKey, ":");
            this.redisKey = String.format("%s:%s:%s", REDIS_ROOT, key, redisKey);
        } else {
            redisKey = String.format("%s:%s", REDIS_ROOT, key);
        }
        return this.redisKey;
    }

}
