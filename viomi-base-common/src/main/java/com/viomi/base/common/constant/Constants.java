package com.viomi.base.common.constant;

/**
 * 常量定义
 *
 * @author ShuBifeng
 * @date 2018-01-04
 */
public final class Constants {

    /**
     * web端与服务端会话token
     */
    public final static String TOKEN = "token";
}
